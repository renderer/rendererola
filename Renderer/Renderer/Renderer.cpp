// Renderer.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include "pch.h"
#include <iostream>
#include "RGB.h"
#include "buffer.h"
#include "TgaBuffer.h"
#include "Rasterizer.h"
#include "float4x4.h"
#include "VertexProcessor.h"
#include "Triangle.h"
#include "Mesh.h"
#include <vector>
#include "ParserOBJ.h"
#include "DirectionalLight.h"
#include "PointLight.h"
#include "TextureMapping.h"
#include "SpotLight.h"

int main()
{
	std::cout << "Hello World!\n";

	float3 ass = float3(3, 3, 3);
	ass.normalize();
	std::cout << ass.ToString();

	TgaBuffer testTgaBuffer = TgaBuffer(512, 512, RGB(55, 155, 255));

	VertexProcessor vp;
	vp.setPerpective(45.0f, 9.0f / 6.0f, 1.0f, 100.0f);
	vp.setIdentityView();

	vp.setLookAt(float3(0, 30.0f, -150.0f), float3(0, 0, -25.0f), float3(0, 1, 0));
	vp.setIdentity();

	vp.tranform();


	Rasterizer ras(testTgaBuffer);
	/*ras.triangle(vp.tr(float3(0.0f, 0.0f, 1.0f)),
		vp.tr(float3(-1.2f, -1.2f, 1.0f)),
		vp.tr(float3(-0.2f, 1.2f, 1.0f))
	);*/

	/*ras.triangle(
		vp.tr(float3(15.0f, 10.0f, 0.0f)),

		vp.tr(float3(-15.0f, 10.0f, 0.0f)),
		vp.tr(float3(0.0f, -8.0f, 0.0f))

	);*/
	/*ras.triangle(
		vp.tr(float3(1.0f, -0.5f, -0.1f)),
		vp.tr(float3(-1.0f, -0.5f, -0.1f)),
		vp.tr(float3(0.0f, 1.0f, 1.0f))
		);*/

	Texture aaa = Texture("./marblebinary.ppm", TextureMappingTypes::PlanarAKARectangularWithZAllignment, 0.2f);
	Texture aaaSphere = Texture("./earthbinary.ppm", TextureMappingTypes::PlanarAKARectangularWithXAllignment, 0.5f);
	Texture aaaSphere2 = Texture("./test2binary.ppm");
	Texture aaaSphere3 = Texture("./earthbinary.ppm");
	Texture aaabb = Texture("./marblebinary.ppm");
	//std::cout << aaa.tg.GetHeight() << " " << aaa.GetWidth() << std::endl;


	PointLight light = PointLight(float3(45, 0, 0), RGB(0, 0, 0), RGB(1, 1, 1), RGB(1, 1, 1), float3(10, 10, 10));
	PointLight light2 = PointLight(float3(5, 0, 0), RGB(0, 0, 0), RGB(255, 0, 155), RGB(255, 0, 0), float3(.01f, .01f, .001f));
	SpotLight light3 = SpotLight(float3(0, 0, -180), RGB(0, 0, 0), RGB(255, 0, 155), RGB(255, 0, 0), float3(0, 0, -1), 25, float3(.3f, .3f, 0.3f));
	//DirectionalLight light2 = DirectionalLight(float3(-15, 0, 0), RGB(0, 0, 0), RGB(0, 155, 155), RGB(0, 155, 155));

	std::vector<Light*> lightsList;
	lightsList.push_back(&light3);
	//lightsList.push_back(&light2);

	Triangle tr1 = Triangle(float3(15.0f, 10.0f, 0.0f),
		float3(-15.0f, 10.0f, 0.0f),
		float3(0.0f, -8.0f, -10.0f));
	//tr1.Draw(ras, vp, light);

	ParserOBJ parser = ParserOBJ();
	std::vector<Mesh> meshes = parser.LoadMesh("./UVteapot5j_000.obj");
	//std::vector<Mesh> meshesSphere = parser.LoadMesh("./UVgeosphere5j000.obj");
	std::vector<Mesh> meshesPiramide = parser.LoadMesh("./UVgeosphere5j000.obj");

	std::vector<Mesh> meshesSphere = parser.LoadMesh("./UVpiramida5j_000.obj");
	std::vector<Mesh> meshesPlane = parser.LoadMesh("./planeUV1x1.obj");


	////vp.multByTransform(float3(0, -20, 0));
	//vp.multByRotation(45, float3(1, 0, 0));
	////vp.multByScale(float3(25.0f, .8f, .8f));

	//std::vector<Mesh>::iterator iterator0;
	//for (iterator0 = meshesPlane.begin(); iterator0 != meshesPlane.end(); ++iterator0)
	//{

	//	std::vector<Triangle>::iterator iteratorTriangle;
	//	for (iteratorTriangle = (*iterator0).meshTriangles.begin(); iteratorTriangle != (*iterator0).meshTriangles.end(); ++iteratorTriangle)
	//	{
	//		iteratorTriangle->texture = &aaa;
	//		iteratorTriangle->Draw3(ras, vp, lightsList);

	//	}
	//}
	//vp.setIdentity();


	vp.multByTransform(float3(0, 0, 0));
	vp.multByScale(float3(5.5f, 5.5f, 5.5f));
	vp.tranform();

	std::vector<Mesh>::iterator iterator;
	for (iterator = meshesSphere.begin(); iterator != meshesSphere.end(); ++iterator)
	{

		std::vector<Triangle>::iterator iteratorTriangle;
		for (iteratorTriangle = (*iterator).meshTriangles.begin(); iteratorTriangle != (*iterator).meshTriangles.end(); ++iteratorTriangle)
		{
			iteratorTriangle->texture = nullptr;
			iteratorTriangle->Draw3(ras, vp, lightsList);

		}
	}

	vp.setIdentity();

	vp.multByTransform(float3(10, 0, 45));
	vp.multByScale(float3(2.0f, 2.0f, 2.0f));
	vp.tranform();

	std::vector<Mesh>::iterator iterator2;
	for (iterator2 = meshes.begin(); iterator2 != meshes.end(); ++iterator2)
	{

		std::vector<Triangle>::iterator iteratorTriangle;
		for (iteratorTriangle = (*iterator2).meshTriangles.begin(); iteratorTriangle != (*iterator2).meshTriangles.end(); ++iteratorTriangle)
		{
			iteratorTriangle->texture = nullptr;
			iteratorTriangle->Draw3(ras, vp, lightsList);

		}
	}
	//vp.setIdentity();
	//vp.multByRotation(180, float3(0, 0, 1));
	//vp.multByTransform(float3(20, 0, 0));
	//vp.multByScale(float3(.6f, .6f, .6f));
	////vp.multByTransform(float3(25, 5, 0));

	//vp.tranform();

	//std::vector<Mesh>::iterator iterator2;
	//for (iterator2 = meshes.begin(); iterator2 != meshes.end(); ++iterator2)
	//{
	//	std::vector<Triangle>::iterator iteratorTriangle;
	//	for (iteratorTriangle = (*iterator2).meshTriangles.begin(); iteratorTriangle != (*iterator2).meshTriangles.end(); ++iteratorTriangle)
	//	{
	//		iteratorTriangle->texture = nullptr;
	//		iteratorTriangle->Draw3(ras, vp, lightsList);

	//	}
	//}

	////vp.setIdentity();
	/////*vp.multByTransform(float3(-25, -5, 0));
	////vp.multByRotation(-180, float3(0, 0, 1));
	////vp.tranform();*/

	////vp.multByTransform(float3(-40, 0, -40));
	////vp.multByScale(float3(.6f, .6f, .6f));
	//////vp.multByRotation(30, float3(0, 1, 1));
	////vp.tranform();

	////std::vector<Mesh>::iterator iterator3;
	////for (iterator3 = meshesPiramide.begin(); iterator3 != meshesPiramide.end(); ++iterator3)
	////{
	////	std::vector<Triangle>::iterator iteratorTriangle;
	////	for (iteratorTriangle = (*iterator3).meshTriangles.begin(); iteratorTriangle != (*iterator3).meshTriangles.end(); ++iteratorTriangle)
	////	{
	////		iteratorTriangle->texture = &aaaSphere2;
	////		iteratorTriangle->Draw3(ras, vp, lightsList);

	////	}
	////}

	////vp.multByTransform(float3(0, 20, -20));
	////vp.multByScale(float3(1.6f, 1.6f, 1.6f));
	////vp.multByRotation(30, float3(0, 1, 1));
	////vp.tranform();

	////std::vector<Mesh>::iterator iterator4;
	////for (iterator4 = meshes.begin(); iterator4 != meshes.end(); ++iterator4)
	////{
	////	std::vector<Triangle>::iterator iteratorTriangle;
	////	for (iteratorTriangle = (*iterator4).meshTriangles.begin(); iteratorTriangle != (*iterator4).meshTriangles.end(); ++iteratorTriangle)
	////	{
	////		iteratorTriangle->texture = nullptr;
	////		iteratorTriangle->Draw2(ras, vp, light3);

	////	}
	////}

	////vp.multByTransform(float3(20, 20, 0));
	////vp.multByScale(float3(1.f, 1.f, 1.f));
	//////vp.multByRotation(30, float3(0, 1, 1));
	////vp.tranform();

	////std::vector<Mesh>::iterator iterator5;
	////for (iterator5 = meshesSphere.begin(); iterator5 != meshesSphere.end(); ++iterator5)
	////{
	////	std::vector<Triangle>::iterator iteratorTriangle;
	////	for (iteratorTriangle = (*iterator5).meshTriangles.begin(); iteratorTriangle != (*iterator5).meshTriangles.end(); ++iteratorTriangle)
	////	{
	////		iteratorTriangle->texture = &aaa;
	////		iteratorTriangle->Draw3(ras, vp, lightsList);

	////	}
	////}

	////vp.multByTransform(float3(-10, 0, 0));
	////////vp.multByScale(float3(.6f, .6f, .6f));
	////vp.tranform();

	////std::vector<Mesh>::iterator iterator6;
	////for (iterator6 = meshesSphere.begin(); iterator6 != meshesSphere.end(); ++iterator6)
	////{

	////	std::vector<Triangle>::iterator iteratorTriangle;
	////	for (iteratorTriangle = (*iterator6).meshTriangles.begin(); iteratorTriangle != (*iterator6).meshTriangles.end(); ++iteratorTriangle)
	////	{
	////		iteratorTriangle->texture = &aaaSphere3;
	////		iteratorTriangle->Draw3(ras, vp, lightsList);

	////	}
	////}

	////vp.multByTransform(float3(+40, -40, 0));
	////vp.multByScale(float3(25.f, 25.f, 25.f));
	//////	vp.multByRotation(180, float3(0, 1, 1));
	////vp.tranform();

	////std::vector<Mesh>::iterator iterator7;
	////for (iterator7 = meshes.begin(); iterator7 != meshes.end(); ++iterator7)
	////{
	////	std::vector<Triangle>::iterator iteratorTriangle;
	////	for (iteratorTriangle = (*iterator7).meshTriangles.begin(); iteratorTriangle != (*iterator7).meshTriangles.end(); ++iteratorTriangle)
	////	{
	////		iteratorTriangle->texture = nullptr;
	////		iteratorTriangle->Draw(ras, vp, light3);

	////	}
	////}


	//ras.triangle(
	//	vp.tr(float3(1.0f, 1.0f, -2.0f)),
	//	vp.tr(float3(0.0f, -1.0f, -2.0f)),
	//	vp.tr(float3(-1.0f, 1.0f, -2.0f))
	//);

	/*ras.triangle(vp.tr(float3(0.8f, 0.4f, 0.8f)),
		vp.tr(float3(-1.2f, -0.8f, 0.8f)),
		vp.tr(float3(0.0f, 0.5f, 0.0f)),
		RGB(115, 25, 189),
		RGB(255, 255, 89),
		RGB(205, 25, 258)
	);*/

	std::cout << "macierz\n";;
	float4x4 aa = float4x4(
		float4(1, 2, 3, 4),
		float4(-1, -2, -3, -4),
		float4(6, 7, 8, 9),
		float4(-6, -7, -8, -9)
	);

	std::string test = aa.toString();
	std::cout << test;

	std::cout << "\nodwrót\n";

	float4x4 tran = aa.transpone();
	test = tran.toString();
	std::cout << test;

	std::cout << "\niloczyn\n";

	float4x4 bbb = aa.mul(tran);

	test = bbb.toString();
	std::cout << test;

	float4 sss = float4(-4, 0.5f, 3.5f, -.25f);
	float4 asd = sss.mul(aa);

	test = asd.ToString();
	std::cout << test;


	testTgaBuffer.SaveFile("test");

	std::cout << "Hello World2!\n";
}

// Uruchomienie programu: Ctrl + F5 lub menu Debugowanie > Uruchom bez debugowania
// Debugowanie programu: F5 lub menu Debugowanie > Rozpocznij debugowanie

// Porady dotyczące rozpoczynania pracy:
//   1. Użyj okna Eksploratora rozwiązań, aby dodać pliki i zarządzać nimi
//   2. Użyj okna programu Team Explorer, aby nawiązać połączenie z kontrolą źródła
//   3. Użyj okna Dane wyjściowe, aby sprawdzić dane wyjściowe kompilacji i inne komunikaty
//   4. Użyj okna Lista błędów, aby zobaczyć błędy
//   5. Wybierz pozycję Projekt > Dodaj nowy element, aby utworzyć nowe pliki kodu, lub wybierz pozycję Projekt > Dodaj istniejący element, aby dodać istniejące pliku kodu do projektu
//   6. Aby w przyszłości ponownie otworzyć ten projekt, przejdź do pozycji Plik > Otwórz > Projekt i wybierz plik sln
