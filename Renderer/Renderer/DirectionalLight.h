#pragma once
#include "Light.h"

class DirectionalLight: public Light
{
public:
	DirectionalLight(float3 position, RGB ambient, RGB diffuse, RGB specular);
	~DirectionalLight();

	virtual RGB calcuate(Vertex vertex, VertexProcessor& vp, Texture * texture = nullptr) override;
	virtual RGB calcuateOnlyColor(Vertex vertex, VertexProcessor & vp) override;
	virtual RGB calcuateOnlyColorSpecalur(Vertex vertex, VertexProcessor & vp) override;
	virtual float calcuateOnlyColorSpotLight(Vertex vertex, VertexProcessor & vp) override;
};

