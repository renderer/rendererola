﻿#include "pch.h"
#include "PointLight.h"


PointLight::PointLight(float3 position, RGB ambient, RGB diffuse, RGB specular, float3 attenstions) : Light(position, ambient, diffuse, specular)
{
	Attenstions = attenstions;
}

PointLight::~PointLight()
{
}

RGB PointLight::calcuate(Vertex vertex, VertexProcessor & vp, Texture * texture)
{
	float3 normalRotate = vp.lt_obj2view(vertex.normal); //macierz3x3
	float3 N = normalRotate.normalizeProduct();

	float4 positionInView = (float4(vertex.position * (-1)).mul(vp.obj2view));
	float3 positionInViewFloat3 = float3(positionInView.GetX(), positionInView.GetY(), positionInView.GetZ());
	float3 V = positionInViewFloat3.normalizeProduct();

	float3 L = position.normalizeProduct() - V;
	L = L.normalizeProduct();
	V = V.normalizeProduct();
	float3 R = (L.reflect(N)).normalizeProduct();
	float diff = clamp01(L.dot(N));
	float spec = pow(clamp01(R.dot(V)), 50);

	if (texture != nullptr)
	{
		RGB newtextureColor = texture->getColor(vertex.UV.getX(), vertex.UV.getY(),vertex.position);
		RGB abc =  diffuse * diff;
		return (ambient+  newtextureColor )* abc + (specular * spec);
		RGB aa=  newtextureColor * RGB(clamp01(abc.getR()), clamp01(abc.getG()), clamp01(abc.getB()))+ (specular * spec);
		return aa;
	}

	return ambient + diffuse * diff + specular * spec;
}




RGB PointLight::calcuateOnlyColor(Vertex vertex, VertexProcessor & vp)
{
	float3 normalRotate = vp.lt_obj2view(vertex.normal); //macierz3x3
	float3 N = normalRotate.normalizeProduct();

	float4 positionInView = (float4(vertex.position * (-1)).mul(vp.obj2view));
	float3 positionInViewFloat3 = float3(positionInView.GetX(), positionInView.GetY(), positionInView.GetZ());
	float3 V = positionInViewFloat3.normalizeProduct();

	float3 L = position.normalizeProduct() - V;
	L = L.normalizeProduct();
	V = V.normalizeProduct();
	float3 R = (L.reflect(N)).normalizeProduct();
	float diff = clamp01(L.dot(N));
	float spec = pow(clamp01(R.dot(V)), 50);

	float3 lightVec = (position) - vertex.position;
	float d = lightVec.length();

	//Atten = 1/( att0i + att1i * d + att2i * d²)
	float att = 1 / (1.0f * (Attenstions.getX() + Attenstions.getY() * d + Attenstions.getZ() *d * d));

	if (Attenstions == float3(0, 0, 0))
	{
		att = 1;
	}
	RGB asdf = ambient + diffuse * diff;
	return ambient + diffuse * diff * att;
}

RGB PointLight::calcuateOnlyColorSpecalur(Vertex vertex, VertexProcessor & vp)
{
	float3 normalRotate = vp.lt_obj2view(vertex.normal); //macierz3x3
	float3 N = normalRotate.normalizeProduct();

	float4 positionInView = (float4(vertex.position * (-1)).mul(vp.obj2view));
	float3 positionInViewFloat3 = float3(positionInView.GetX(), positionInView.GetY(), positionInView.GetZ());
	float3 V = positionInViewFloat3.normalizeProduct();

	float3 L = position.normalizeProduct() - V;
	L = L.normalizeProduct();
	V = V.normalizeProduct();
	float3 R = (L.reflect(N)).normalizeProduct();
	float diff = clamp01(L.dot(N));
	float spec = pow(clamp01(R.dot(V)), 50);

	float3 lightVec = (position) - vertex.position;

	float d = lightVec.length();

	//Atten = 1/( att0i + att1i * d + att2i * d²)
	float att = 1 / (1.0f * (Attenstions.getX() + Attenstions.getY() * d + Attenstions.getZ() *d * d));

	if (Attenstions == float3(0, 0, 0))
	{
		att = 1;
	}

	RGB asasf = specular * spec;
	return specular * spec *att;
}

float PointLight::calcuateOnlyColorSpotLight(Vertex vertex, VertexProcessor & vp)
{
	return 1.0f;
}
