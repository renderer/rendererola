#include "pch.h"
#include "float4x4.h"


float4x4::float4x4()
{
	column[0] = float4();
	column[1] = float4();
	column[2] = float4();
	column[3] = float4();

}

float4x4::float4x4(float4 a, float4 b, float4 c, float4 d)
{
	column[0] = a;
	column[1] = b;
	column[2] = c;
	column[3] = d;
}


float4x4::~float4x4()
{
}

float4x4 float4x4::mul(float4x4 b) {

	float4x4 aTr = this->transpone();
	float4x4 r = float4x4(
		float4(aTr.column[0].dot(b.column[0]), aTr.column[1].dot(b.column[0]), aTr.column[2].dot(b.column[0]), aTr.column[3].dot(b.column[0])),
		float4(aTr.column[0].dot(b.column[1]), aTr.column[1].dot(b.column[1]), aTr.column[2].dot(b.column[1]), aTr.column[3].dot(b.column[1])),
		float4(aTr.column[0].dot(b.column[2]), aTr.column[1].dot(b.column[2]), aTr.column[2].dot(b.column[2]), aTr.column[3].dot(b.column[2])),
		float4(aTr.column[0].dot(b.column[3]), aTr.column[1].dot(b.column[3]), aTr.column[2].dot(b.column[3]), aTr.column[3].dot(b.column[3])));

	return r;
}

float4x4 float4x4::transpone()
{
	float4 row0 = float4(column[0].x, column[1].x, column[2].x, column[3].x);
	float4 row1 = float4(column[0].y, column[1].y, column[2].y, column[3].y);
	float4 row2 = float4(column[0].z, column[1].z, column[2].z, column[3].z);
	float4 row3 = float4(column[0].w, column[1].w, column[2].w, column[3].w);

	return float4x4(row0, row1, row2, row3);
}

std::string float4x4::toString()
{
	std::string r = "";

	r += "[" + std::to_string(column[0].x) + ", " + std::to_string(column[1].x) + ", " + std::to_string(column[2].x) + ", " + std::to_string(column[3].x) + "]\n";
	r += "[" + std::to_string(column[0].y) + ", " + std::to_string(column[1].y) + ", " + std::to_string(column[2].y) + ", " + std::to_string(column[3].y) + "]\n";
	r += "[" + std::to_string(column[0].z) + ", " + std::to_string(column[1].z) + ", " + std::to_string(column[2].z) + ", " + std::to_string(column[3].z) + "]\n";
	r += "[" + std::to_string(column[0].w) + ", " + std::to_string(column[1].w) + ", " + std::to_string(column[2].w) + ", " + std::to_string(column[3].w) + "]\n";


	return r;
}
