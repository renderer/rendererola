#pragma once
#include "float3.h"
#include "float4x4.h"
class VertexProcessor
{

public:
	float4x4 obj2world;
	float4x4 world2view;
	float4x4 view2proj;
	float4x4 obj2proj;
	float4x4 obj2view;


	VertexProcessor();
	~VertexProcessor();

	float3 tr(float3 v);
	float3 lt();
	void setPerpective(float fovy, float aspect, float near, float far);
	void setLookAt(float3 eye, float3 center, float3 up);
	void setIdentity();
	void setIdentityView();
	void multByTransform(float3 v);
	void multByScale(float3 v);
	void multByRotation(float a, float3 v);
	void tranform();

	float3 lt_obj2view(float3 normal);
};

