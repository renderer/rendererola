#pragma once
#include <vector>
#include "float3.h"
#include "Mesh.h"

class ParserOBJ
{
public:
	ParserOBJ();
	~ParserOBJ();
	std::vector<Mesh> LoadMesh(const std::string &fileName);
	std::vector<std::string> splitString(std::string &line, std::string delim);
	void parsefloat3(std::string& line, std::vector<float3>& vectorCollection);
	void parseV(std::string & line, std::vector<float3>& points, std::vector<float3>& normals, std::vector<float3>& textures);
	//void parseF(std::string & line, Mesh *& currentMesh, std::vector<float3>& points, std::vector<float3>& normals, std::string & currentGroupName);
	Triangle parseF(std::string line, std::vector<float3>& points, std::vector<float3>& normals, std::vector<float3>& textures);
	std::string PardeG(std::string &line);
};

