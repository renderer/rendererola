#pragma once
#include "Buffer.h"
#include <fstream>
#include <stdio.h>
#include <list>

class TgaBuffer : public Buffer
{
public:
	TgaBuffer();
	TgaBuffer(int width, int height);
	TgaBuffer(int width, int height, RGB color);
	TgaBuffer(const std::string& name);
	~TgaBuffer();
	void SaveFile(std::string fileName);
	void Load(std::string filename, bool binary);

private:
	//unsigned int m_Header[9] =
	//{
	//	0x0000, 0x0002, 0x0000,
	//	0x0000, 0x0000, 0x0000,
	//	0x0100, 0x0100, 0x0820 // width, height, ...
	//};
};


static class Extensions {
public:
	static std::list<std::string> splitStr(std::string str, char character) {
		std::list<std::string> ret;

		std::string tempstring = "";
		for (int i = 0; i < str.length(); i++)
		{
			if (str[i] == character || str[i] == '\0') {
				ret.push_back(std::string(tempstring));
				tempstring.clear();
			}
			else {
				tempstring.push_back(str[i]);
			}
		}
		ret.push_back(std::string(tempstring));
		return ret;
	}

	static std::string Trim(std::string str, char character) {

		std::string ret = "";
		bool done = false;
		int indbegin = 0, indend = str.length() - 1;

		while (!done) {
			done = true;
			if (str[indbegin] == character) {
				indbegin++;
				done = false;
			}
			if (str[indend] == character) {
				indend--;
				done = false;
			}
		}

		for (int i = indbegin; i <= indend; i++)
		{
			ret.push_back(str[i]);
		}
		return ret;
	}

	static int charToInt(char *str) {
		int retint = 0, tmp = 0, pot = 1;
		int len = strlen(str);
		for (int i = 0; i < len; i++) {
			tmp = str[len - 1 - i] - 48;
			pot = 1;
			for (int j = 0; j < i; j++) pot *= 10;
			retint += tmp * pot;
		}
		return retint;
	}

	static int charToInt(std::string str) {
		int retint = 0, tmp = 0, pot = 1;
		int len = str.length();
		for (int i = 0; i < len; i++) {
			tmp = str[len - 1 - i] - 48;
			pot = 1;
			for (int j = 0; j < i; j++) pot *= 10;
			retint += tmp * pot;
		}
		return retint;
	}

	static float Clamp(float variable, float min = 0.0f, float max = 1.0f) {
		if (variable < min) return min;
		else if (variable > max) return max;
		return variable;
	}
};
