#include "pch.h"

#include "TextureMappingRectangular.h"


TextureMappingRectangular::TextureMappingRectangular(float scale, Allignment allignment) : TextureMapping(scale)
{
	this->allignment = allignment;
}


TextureMappingRectangular::~TextureMappingRectangular()
{
}

void TextureMappingRectangular::getPixelCoordinates(float3 & localHitPoint, const unsigned int width, const unsigned int height, int & row, int & column)
{

	float u;
	float v;
	switch (allignment)
	{
	case X:
		u = 1.0F - ((localHitPoint.getZ() + 1) / 2.0F);
		v = (localHitPoint.getY() + 1) / 2.0F;
		break;
	case Y:
		u = 1.0F - ((localHitPoint.getX() + 1) / 2.0F);
		v = (localHitPoint.getZ() + 1) / 2.0F;
		break;
	case Z:
		u = 1.0F - ((localHitPoint.getX() + 1) / 2.0F);
		v = (localHitPoint.getY() + 1) / 2.0F;
		break;
	default:
		break;
	}
	u = u > 1 ? 1 : u;
	v = v > 1 ? 1 : v;

	getRowAndColumn(u, v, width, height, row, column);

}
