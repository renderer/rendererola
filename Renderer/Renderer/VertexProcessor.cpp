#include "pch.h"
#include "VertexProcessor.h"
#include <iostream>

#define M_PI       3.14159265358979323846


VertexProcessor::VertexProcessor()
{
}


VertexProcessor::~VertexProcessor()
{
}

float3 VertexProcessor::tr(float3 v)
{
	float4 r = float4(v, 1).mul(obj2proj);
	//std::cout << r.ToString() << "\n";
	return float3(r.x / r.w, r.y / r.w, r.z / r.w);

}

float3 VertexProcessor::lt()
{
	return float3();
}

void VertexProcessor::setPerpective(float fovy, float aspect, float near, float far)
{
	fovy = fovy * M_PI / 360.0f;
	float f = cos(fovy) / sin(fovy);
	view2proj.column[0] = float4(f / aspect, 0, 0, 0);
	view2proj.column[1] = float4(0, f, 0, 0);
	view2proj.column[2] = float4(0, 0, (far + near) / ( 1.0f *(near - far)), -1);
	view2proj.column[3] = float4(0, 0, 2 * far*near / (near - far), 0);
}

void VertexProcessor::setLookAt(float3 eye, float3 center, float3 up)
{
	float3 f = center - eye;
	f = f.normalizeProduct();
	up = up.normalizeProduct();
	float3 s = f.cross(up);
	float3 u = s.cross(f);
	world2view.column[0] = float4(s.x, u.x, -f.x, 0);
	world2view.column[1] = float4(s.y, u.y, -f.y, 0);
	world2view.column[2] = float4(s.z, u.z, -f.z, 0);
	world2view.column[3] = float4(eye * (-1), 1);

}

void VertexProcessor::setIdentity()
{
	obj2world.column[0] = float4(1, 0, 0, 0);
	obj2world.column[1] = float4(0, 1, 0, 0);
	obj2world.column[2] = float4(0, 0, 1, 0);
	obj2world.column[3] = float4(0, 0, 0, 1);
}

void VertexProcessor::setIdentityView()
{
	world2view.column[0] = float4(1, 0, 0, 0);
	world2view.column[1] = float4(0, 1, 0, 0);
	world2view.column[2] = float4(0, 0, 1, 0);
	world2view.column[3] = float4(0, 0, 0, 1);
}

void VertexProcessor::multByTransform(float3 v)
{
	float4x4 m = float4x4(
		float4(1, 0, 0, 0),
		float4(0, 1, 0, 0),
		float4(0, 0, 1, 0),
		float4(v.x, v.y, v.z, 1));

	obj2world = m.mul(obj2world);
}

void VertexProcessor::multByScale(float3 v)
{
	float4x4 m = float4x4(
		float4(v.x, 0, 0, 0),
		float4(0, v.y, 0, 0),
		float4(0, 0, v.z, 0),
		float4(0, 0, 0, 1));

	obj2world = m.mul(obj2world);
}

void VertexProcessor::multByRotation(float a, float3 v)
{
	float s = sin(a *M_PI / 180);
	float c = cos(a*M_PI / 180);
	v = v.normalizeProduct();

	float4x4 m = float4x4(
		float4(v.x* v.x*(1 - c) + c, v.y*v.x*(1 - c) + v.z*s, v.x*v.z*(1 - c) - v.y*s, 0),
		float4(v.x*v.y*(1 - c) - v.z*s, v.y*v.y*(1 - c) + c, v.y*v.z*(1 - c) + v.x*s, 0),
		float4(v.x*v.z*(1 - c) + v.y*s, v.y*v.z*(1 - c) - v.x*s, v.z*v.z*(1 - c) + c, 0),
		float4(0, 0, 0, 1)
	);


	/*float4x4 m = float4x4(
		float4(v.x* v.x*(1.0 - c) + c, v.y*v.x*(1.0 - c) - v.z*s, v.x*v.z*(1.0 - c) + v.y*s, 0),
		float4(v.x*v.y*(1.0 - c) + v.z*s, v.y*v.y*(1.0 - c) + c, v.y*v.z*(1.0 - c) - v.x*s, 0),
		float4(v.x*v.z*(1.0 - c) - v.y*s, v.y*v.z*(1.0 - c) + v.x*s, v.z*v.z*(1.0 + c) + c, 0),
		float4(0, 0, 0, 1.0)
	);*/

	obj2world = m.mul(obj2world);


}

void VertexProcessor::tranform()
{
	std::cout << "world2view \n";
	std::cout << world2view.toString();

	std::cout << "obj2world \n";
	std::cout << obj2view.toString();

	std::cout << "view2proj\n";
	std::cout << view2proj.toString();

	std::cout << "obj2View \n";
	std::cout << obj2view.toString();

	obj2view = world2view.mul(obj2world);

	std::cout << "\n \n Po po po obliczeniach \n obj2View \n";
	std::cout << obj2view.toString();

	obj2proj = view2proj.mul(obj2view);

	std::cout << "\n \n Po po po obliczeniach \n obj2proj \n";
	std::cout << obj2proj.toString();
}

float3 VertexProcessor::lt_obj2view(float3 normal)
{
	float3 newFloat3 = float3();
	float3 row0 = float3(obj2view.column[0].x, obj2view.column[1].x, obj2view.column[2].x);
	float3 row1 = float3(obj2view.column[0].y, obj2view.column[1].y, obj2view.column[2].y);
	float3 row2 = float3(obj2view.column[0].z, obj2view.column[1].z, obj2view.column[2].z);

	newFloat3.x = normal.dot(row0);
	newFloat3.y = normal.dot(row1);
	newFloat3.z = normal.dot(row2);

	return newFloat3;
}
