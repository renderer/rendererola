﻿#include "pch.h"
#include "SpotLight.h"
#define M_PI       3.14159265358979323846

SpotLight::SpotLight(float3 position, RGB ambient, RGB diffuse, RGB specular, float3 direction, float angle, float3 attenstions) : Light(position, ambient, diffuse, specular)
{
	Direction = direction;
	Angle = angle;
	Attenstions = attenstions;
}

SpotLight::~SpotLight()
{
}

RGB SpotLight::calcuate(Vertex vertex, VertexProcessor & vp, Texture * texture)
{
	return RGB();
}

RGB SpotLight::calcuateOnlyColor(Vertex vertex, VertexProcessor & vp)
{
	float3 L = position.normalizeProduct();
	float3 normalRotate = vp.lt_obj2view(vertex.normal); //macierz3x3
	float3 N = normalRotate.normalizeProduct();

	float4 positionInView = (float4(vertex.position * (-1)).mul(vp.obj2view));
	float3 positionInViewFloat3 = float3(positionInView.GetX(), positionInView.GetY(), positionInView.GetZ());
	float3 V = positionInViewFloat3.normalizeProduct();

	
	float3 lightVec = position.normalizeProduct() - vertex.position.normalizeProduct();
	float d = lightVec.length();
	float3 negativeL = L;
	negativeL.negate();

	float cosA = ((Direction.normalizeProduct()).dot(lightVec.normalizeProduct()));
	if (abs(cosA) < cos(Angle *M_PI / 180))
	{
		return RGB();
	}

	//float3 R = (L.reflect(N)).normalizeProduct(); ((Direction.normalizeProduct()).reflect(N)).normalizeProduct();
	float diff = clamp01((Direction.normalizeProduct()).dot(N));
	//float aa = R.dot(V);

	//float spec = pow(clamp01(R.dot(V)), 50);
	//float spec = aa > 0 ? aa : -aa;

	//Atten = 1/( att0i + att1i * d + att2i * d²)
	float att = 1 / (1.0f * (Attenstions.getX() + Attenstions.getY() * d + Attenstions.getZ() *d * d));

	if (Attenstions == float3(0, 0, 0))
	{
		att = 1;
	}


	return ambient + diffuse * diff * att;
}

RGB SpotLight::calcuateOnlyColorSpecalur(Vertex vertex, VertexProcessor & vp)
{
	float3 L = position.normalizeProduct();
	float3 normalRotate = vp.lt_obj2view(vertex.normal); //macierz3x3
	float3 N = normalRotate.normalizeProduct();

	float4 positionInView = (float4(vertex.position * (-1)).mul(vp.obj2view));
	float3 positionInViewFloat3 = float3(positionInView.GetX(), positionInView.GetY(), positionInView.GetZ());
	float3 V = positionInViewFloat3.normalizeProduct();

	float3 R = ((Direction.normalizeProduct()).reflect(N)).normalizeProduct();
	float spec = pow(clamp01(R.dot(V)), 50);
	//float spec = aa > 0 ? aa : -aa;
	
	float3 lightVec = position.normalizeProduct() - vertex.position.normalizeProduct();

	float d = lightVec.length();
	float3 negativeL = L;
	negativeL.negate();

	float cosA = ((Direction.normalizeProduct()).dot(lightVec.normalizeProduct()));
	if (abs(cosA) < cos(Angle *M_PI / 180))
	{
		return RGB();
	}



	//Atten = 1/( att0i + att1i * d + att2i * d²)
	float att = 1 / (1.0f * (Attenstions.getX() + Attenstions.getY() * d + Attenstions.getZ() *d * d));

	if (Attenstions == float3(0, 0, 0))
	{
		att = 1;
	}


	return  specular * spec * att;
}

float SpotLight::calcuateOnlyColorSpotLight(Vertex vertex, VertexProcessor & vp)
{
	float4 positionInView = (float4(vertex.position * (-1)).mul(vp.obj2view));
	float3 positionInViewFloat3 = float3(positionInView.GetX(), positionInView.GetY(), positionInView.GetZ());
	float3 V = positionInViewFloat3.normalizeProduct();

	float3 lightVec = position.normalizeProduct() - vertex.position.normalizeProduct();


	float cosA = ((Direction.normalizeProduct()).dot(lightVec.normalizeProduct()));

	std::cout << "kat " << cosA << std::endl;

	return pow(std::max(abs(cosA), 0.0f), 20);


}
