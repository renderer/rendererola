#pragma once
#include "Light.h"
class PointLight : public Light
{
public:
	float3 Attenstions;
	PointLight(float3 position, RGB ambient, RGB diffuse, RGB specular, float3 attenstions);
	~PointLight();

	virtual RGB calcuate(Vertex vertex, VertexProcessor& vp, Texture * texture = nullptr) override;
	virtual RGB calcuateOnlyColor(Vertex vertex, VertexProcessor & vp) override;
	virtual RGB calcuateOnlyColorSpecalur(Vertex vertex, VertexProcessor & vp) override;
	virtual float calcuateOnlyColorSpotLight(Vertex vertex, VertexProcessor & vp) override;

};

