#pragma once
#include <string>
#include "TgaBuffer.h"

#include "TextureMappingRectangular.h"
#include "TextureMappingSpherical.h"


class Texture
{
private:
	TgaBuffer image;
	TextureMapping* textureMapping;
	
public:
	Texture();
	Texture(const std::string &fileName);

	Texture(const std::string &fileName, TextureMappingTypes textureMappingType, float textureScale);
	RGB getColor(const float & u, const float & v, float3 hit);
	void getRowAndColumn(const float & u, const float & v, const unsigned int & width, const unsigned int & height, int & row, int & column);

	//RGBA getColor(Vector3 & localHitPoint);
	//float getScale();

	virtual ~Texture();
};
