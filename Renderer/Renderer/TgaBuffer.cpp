#include "pch.h"
#include "TgaBuffer.h"


TgaBuffer::TgaBuffer() : Buffer()
{
}

TgaBuffer::TgaBuffer(int width, int height) : Buffer(width, height)
{
}

TgaBuffer::TgaBuffer(int width, int height, RGB color) : Buffer(width, height, color)
{
}

TgaBuffer::TgaBuffer(const std::string & name)
{

	SetWidth(0);
	SetHeight(0);
	
	Load(name, true);
}


TgaBuffer::~TgaBuffer()
{
}

void TgaBuffer::SaveFile(std::string fileName)
{
	// Save result to a PPM image (keep these flags if you compile under Windows)
	std::ofstream ofs("./" + fileName + "out2.ppm", std::ios::out | std::ios::binary);
	ofs << "P6\n" << GetWidth() << " " << GetHeight() << "\n255\n";
	for (uint32_t i = 0; i < GetLength(); ++i) {
		char r = (char)(VectorsColor[i].getR());
		char g = (char)(VectorsColor[i].getG());
		char b = (char)(VectorsColor[i].getB());
		ofs << r << g << b;

	}

	ofs.close();

	//m_Header[6] = GetWidth();
	//m_Header[7] = GetHeight();

	//fileName = fileName + ".tga";

	//FILE *tga_file = nullptr;
	//fopen_s(&tga_file, fileName.c_str(), "wb+");

	//fwrite(m_Header, 2, 9, tga_file);
	//fwrite(VectorsColor.data(), 3, GetLength(), tga_file);

	//fclose(tga_file);

}



void TgaBuffer::Load(std::string filename, bool binary) {

	VectorsColor.resize(1);
	std::ifstream plik_we;
	if (!binary)	plik_we.open(filename);
	else	plik_we.open(filename, std::ios::binary);
	if (!plik_we.is_open()) {
		std::cerr << "File cannot be opened.\n";
		exit(1);
	}
	std::string line;

	for (int i = 0; i < 3 && !plik_we.eof(); i++) {
		std::getline(plik_we, line);
		if (line[0] == '#') {
			i--;
		}
		else if (i == 1) {
			std::list<std::string> lineSplitted = Extensions::splitStr(line, ' ');
			int width = Extensions::charToInt(lineSplitted.front());
			int height = Extensions::charToInt(lineSplitted.back());
			SetWidth(width);
			SetHeight(height);
		}
	}

	VectorsColor.resize(GetWidth() * GetHeight());


	if (!binary) {
		int x = 0, y = 0;
		std::getline(plik_we, line);
		while (!plik_we.eof() && x < GetWidth() && y < GetHeight()) {
			int r, g, b;
			int i = 0;
			line = Extensions::Trim(line, ' ');
			std::list<std::string> lineSplitted = Extensions::splitStr(line, ' ');
			std::list<std::string>::iterator iterator;
			for (iterator = lineSplitted.begin(); iterator != lineSplitted.end(); ++iterator) {
				if (i == 0) {
					r = Extensions::charToInt(*iterator);
					i = 1;
				}
				else if (i == 1) {
					g = Extensions::charToInt(*iterator);
					i = 2;
				}
				else if (i == 2) {
					b = Extensions::charToInt(*iterator);
					i = 0;
					SetColorInPixel(x, y, RGB(r, g, b));
					x++;
				}
			}
			if (x >= GetWidth())
			{
				x = 0;
				y++;
				std::cout << y << ' ';
			}
			if (y >= GetHeight()) y = 0;
			std::getline(plik_we, line);
		}
	}
	else {

		int x = 0, y = 0;
		int r, g, b;
		char temp;
		for (int i = 0; i < GetWidth() * GetHeight(); i++) {
			plik_we.read(&temp, 1);
			r = (unsigned char)temp;
			plik_we.read(&temp, 1);
			g = (unsigned char)temp;
			plik_we.read(&temp, 1);
			b = (unsigned char)temp;
			SetColorInPixel(x, y, RGB(r, g, b));
			x++;
			if (x >= GetHeight())
			{
				x = 0;
				y++;
			}
			if (y >= GetHeight()) y = 0;
		}

	}
	plik_we.close();
}