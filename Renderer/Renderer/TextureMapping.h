#pragma once
#define _USE_MATH_DEFINES
#include "float3.h"
#include <cmath>


class TextureMapping
{
protected:
	float scale;
	float calculatePhi(float3& localHitPoint);

public:
	TextureMapping(float scale);

	float getScale() const;
	void setScale(float scale);

	void getRowAndColumn(const float& u, const float& v, const unsigned int& width, const unsigned int& height, int& row, int& column);

	virtual void getPixelCoordinates(float3& localHitPoint, const unsigned int width, const unsigned int height, int& row, int& column) = 0;


	virtual ~TextureMapping();
};

enum TextureMappingTypes
{
	PlanarAKARectangularWithXAllignment,
	PlanarAKARectangularWithYAllignment,
	PlanarAKARectangularWithZAllignment,
	Spherical
};

