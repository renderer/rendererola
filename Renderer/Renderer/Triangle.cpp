#include "pch.h"
#include "Triangle.h"


Triangle::Triangle()
{
	va = Vertex();
	vb = Vertex();
	vc = Vertex();
	this->texture = nullptr;

}

Triangle::Triangle(float3 a, float3 b, float3 c, float3 normal)
{

	this->normalVector = normal.normalizeProduct();
	va = Vertex(a, normalVector, RGB(255, 255, 255), float3());
	vb = Vertex(b, normalVector, RGB(255, 255, 255), float3());
	vc = Vertex(c, normalVector, RGB(255, 255, 255), float3());
	this->texture = nullptr;

}

Triangle::Triangle(float3 a, float3 b, float3 c)
{
	this->normalVector = ((b - a).cross(c - a)).normalizeProduct();

	va = Vertex(a, normalVector, RGB(255, 255, 255), float3());
	vb = Vertex(b, normalVector, RGB(255, 255, 255), float3());
	vc = Vertex(c, normalVector, RGB(255, 255, 255), float3());
	this->texture = nullptr;

}


Triangle::Triangle(float3 a, float3 b, float3 c, float3 Na, float3 Nb, float3 Nc, float3 Ua, float3 Ub, float3 Uc)
{
	va = Vertex(a, Na, RGB(255, 255, 255), Ua);
	vb = Vertex(b, Nb, RGB(255, 255, 255), Ub);
	vc = Vertex(c, Nc, RGB(255, 255, 255), Uc);
	this->normalVector = ((b - a).cross(c - a)).normalizeProduct();
	this->texture = nullptr;

}

Triangle::Triangle(float3 a, float3 b, float3 c, float3 Na, float3 Nb, float3 Nc, float3 Ua, float3 Ub, float3 Uc, Texture* texture)
{
	va = Vertex(a, Na, RGB(255, 255, 255), Ua);
	vb = Vertex(b, Nb, RGB(255, 255, 255), Ub);
	vc = Vertex(c, Nc, RGB(255, 255, 255), Uc);
	this->normalVector = ((b - a).cross(c - a)).normalizeProduct();
	this->texture = texture;
}


Triangle::~Triangle()
{
}

void Triangle::Draw(Rasterizer & r, VertexProcessor & vp, Light & light)
{
	if (texture != nullptr)
	{
		

		r.triangle(
			{ vp.tr(va.position), va.normal, light.calcuate({ va.position,va.normal }, vp, texture), va.UV },
			{ vp.tr(vb.position), vb.normal, light.calcuate({ vb.position,vb.normal }, vp,texture), vb.UV },
			{ vp.tr(vc.position), vc.normal, light.calcuate({ vc.position,vc.normal }, vp,texture), vc.UV }
		);
	}
	else
	{
		va.color = RGB(255, 0, 0);
		vb.color = RGB(0, 255, 0);
		vc.color = RGB(0, 0, 255);
		r.triangle(
			{ vp.tr(va.position), va.normal, light.calcuate({ va.position,va.normal }, vp) },
			{ vp.tr(vb.position), vb.normal,light.calcuate({ va.position,va.normal }, vp) },
			{ vp.tr(vc.position), vc.normal, light.calcuate({ va.position,va.normal }, vp) }
		);
	}
}


void Triangle::Draw2(Rasterizer & r, VertexProcessor & vp, Light & light)
{
	std::cout << va.ToString();
	std::cout << vb.ToString();
	std::cout << vc.ToString();

	if (texture != nullptr)
	{
		r.triangle2(
			{ vp.tr(va.position), va.normal,light.calcuate({ va.position,va.normal }, vp,texture), va.UV },
			{ vp.tr(vb.position), vb.normal,light.calcuate({ vb.position,vb.normal }, vp,texture), vb.UV },
			{ vp.tr(vc.position), vc.normal,light.calcuate({ vc.position,vc.normal }, vp,texture), vc.UV },
			vp, light, texture
		);
	}
	else {

		r.triangle2(
			{ vp.tr(va.position), va.normal,RGB(255,0,0), va.UV },
			{ vp.tr(vb.position), vb.normal,RGB(0,255,0), vb.UV },
			{ vp.tr(vc.position), vc.normal,RGB(0,0,255), vc.UV },
			vp, light
		);
	}
}

void Triangle::Draw3(Rasterizer & r, VertexProcessor & vp, std::vector<Light*> lights)
{
	if (texture != nullptr)
	{
		r.triangle3(
			{ vp.tr(va.position), va.normal,RGB(255,0,0), va.UV },
			{ vp.tr(vb.position), vb.normal,RGB(0,255,0), vb.UV },
			{ vp.tr(vc.position), vc.normal,RGB(0,0,255), vc.UV },
			vp, lights, texture
		);
	}
	else {

		r.triangle3(
			{ vp.tr(va.position), va.normal,RGB(255,0,0), va.UV },
			{ vp.tr(vb.position), vb.normal,RGB(0,255,0), vb.UV },
			{ vp.tr(vc.position), vc.normal,RGB(0,0,255), vc.UV },
			vp, lights
		);
	}
}

//void Triangle::Draw(Rasterizer & r, VertexProcessor & vp)
//{
//	r.triangle(
//		{ vp.tr(va.position),float3(),texture->getColor(va.UV.getX(), va.UV.getY()) },
//		{ vp.tr(vb.position),float3(),texture->getColor(vb.UV.getX(), vb.UV.getY()) },
//		{ vp.tr(vc.position),float3(),texture->getColor(vc.UV.getX(), vc.UV.getY()) }
//	);
//}


void Triangle::Draw(Rasterizer & r, VertexProcessor & vp)
{
	if (texture != nullptr)
	{

		r.triangle(
			{ vp.tr(va.position),float3(),RGB(255,0,0) },
			{ vp.tr(vb.position),float3(),RGB(0,255,0) },
			{ vp.tr(vc.position),float3(),RGB(0,0,255) }
		);
	}
	else
	{
		r.triangle(
			{ vp.tr(va.position),float3(),texture->getColor(va.UV.getX(), va.UV.getY(), va.position) },
			{ vp.tr(vb.position),float3(),texture->getColor(vb.UV.getX(), vb.UV.getY(), vb.position) },
			{ vp.tr(vc.position),float3(),texture->getColor(vc.UV.getX(), vc.UV.getY(), vc.position) }
		);
	}

}
//
//
//void Triangle::Draw3(Rasterizer & r, VertexProcessor & vp)
//{
//	if (texture != nullptr)
//	{
//
//		r.triangle3(
//			{ vp.tr(va.position),float3(),RGB(255,0,0) },
//			{ vp.tr(vb.position),float3(),RGB(0,255,0) },
//			{ vp.tr(vc.position),float3(),RGB(0,0,255) }
//		);
//	}
//	else
//	{
//		r.triangle3(
//			{ vp.tr(va.position),float3(),texture->getColor(va.UV.getX(), va.UV.getY()) },
//			{ vp.tr(vb.position),float3(),texture->getColor(vb.UV.getX(), vb.UV.getY()) },
//			{ vp.tr(vc.position),float3(),texture->getColor(vc.UV.getX(), vc.UV.getY()) }
//		);
//	}
//
//}


float Triangle::CalculateSurfaceArea()
{
	float aa = (vb.position - va.position).length();
	float bb = (vc.position - va.position).length();
	float cc = (vc.position - vb.position).length();
	float p = (aa + bb + cc) / 2.0f;
	return sqrt(p * (p - aa)*(p - bb)* (p - cc));
}
