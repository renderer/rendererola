#pragma once
#include "TextureMapping.h"
enum Allignment;

class TextureMappingRectangular : public TextureMapping
{
private:
	Allignment allignment;
public:

	TextureMappingRectangular(float scale, Allignment allignment);
	virtual ~TextureMappingRectangular();

	// Inherited via TextureMapping
	virtual void getPixelCoordinates(float3 & localHitPoint, const unsigned int width, const unsigned int height, int & row, int & column) override;
};

enum Allignment
{
	X,
	Y,
	Z
};

