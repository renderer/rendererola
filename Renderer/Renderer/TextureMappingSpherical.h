#pragma once

#include "TextureMapping.h"
class TextureMappingSpherical : public TextureMapping
{
public:
	TextureMappingSpherical(float scale);
	virtual ~TextureMappingSpherical();

	// Inherited via TextureMapping
	virtual void getPixelCoordinates(float3 & localHitPoint, const unsigned int width, const unsigned int height, int & row, int & column) override;
};
