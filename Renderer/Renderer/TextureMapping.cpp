#include "pch.h"
#include "TextureMapping.h"

float TextureMapping::calculatePhi(float3 & localHitPoint)
{
	float phi = atan2f(localHitPoint.getX(), localHitPoint.getZ());
	if (phi < 0.0F)
	{
		phi += 2.0F * (float)M_PI;
	}
	return phi;
}

TextureMapping::TextureMapping(float scale)
{
	this->scale = scale;
}

float TextureMapping::getScale() const
{
	return this->scale;
}

void TextureMapping::setScale(float scale)
{
	this->scale = scale;
}

void TextureMapping::getRowAndColumn(const float & u, const float & v, const unsigned int & width, const unsigned int & height, int & row, int & column)
{
	row = (int)((width - 1) * u);
	column = (int)((height - 1) * v);
}

TextureMapping::~TextureMapping()
{
}