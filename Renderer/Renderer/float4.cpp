#include "pch.h"
#include "float4x4.h"
#include "float4.h"
#include "float3.h"

float4::~float4()
{
}

float4::float4()
{
	x = 0.0f;
	y = 0.0f;
	z = 0.0f;
	w = 0.0f;
};

float4::float4(float _x, float _y, float _z, float _w)
{
	this->x = _x;
	this->y = _y;
	this->z = _z;
	this->w = _w;
}

float4::float4(float3 value)
{
	this->x = value.x;
	this->y = value.y;
	this->z = value.z;
	this->w = 1;
}

float4::float4(float3 value, float w)
{
	this->x = value.x;
	this->y = value.y;
	this->z = value.z;
	this->w = w;
}


float float4::GetX()
{
	return this->x;
}
float float4::GetY()
{
	return this->y;
}
float float4::GetZ()
{
	return this->z;
}
float float4::GetW()
{
	return this->w;
}

void float4::SetX(float value)
{
	this->x = value;
}
void float4::SetY(float value)
{
	this->y = value;
}
void float4::SetZ(float value)
{
	this->z = value;
}
void float4::SetW(float value)
{
	this->w = value;
}

float4 float4::mul(float4x4 aa)
{
	float4 newFloat4 = float4();
	float4 row0 = float4(aa.column[0].x, aa.column[1].x, aa.column[2].x, aa.column[3].x);
	float4 row1 = float4(aa.column[0].y, aa.column[1].y, aa.column[2].y, aa.column[3].y);
	float4 row2 = float4(aa.column[0].z, aa.column[1].z, aa.column[2].z, aa.column[3].z);
	float4 row3 = float4(aa.column[0].w, aa.column[1].w, aa.column[2].w, aa.column[3].w);

	newFloat4.x = this->dot(row0);
	newFloat4.y = this->dot(row1);
	newFloat4.z = this->dot(row2);
	newFloat4.w = this->dot(row3);

	return newFloat4;
}
float4 float4::mul(float4x4 aa, float3 vector)
{
	float4 vector4 = float4(vector);
	float4 newFloat4 = float4();
	float4 row0 = float4(aa.column[0].x, aa.column[1].x, aa.column[2].x, aa.column[3].x);
	float4 row1 = float4(aa.column[0].y, aa.column[1].y, aa.column[2].y, aa.column[3].y);
	float4 row2 = float4(aa.column[0].z, aa.column[1].z, aa.column[2].z, aa.column[3].z);
	float4 row3 = float4(aa.column[0].w, aa.column[1].w, aa.column[2].w, aa.column[3].w);

	newFloat4.x = vector4.dot(row0);
	newFloat4.y = vector4.dot(row1);
	newFloat4.z = vector4.dot(row2);
	newFloat4.w = vector4.dot(row3);

	return newFloat4;
}



std::string float4::ToString()
{
	return "float4(" + std::to_string(this->x) + ";" + std::to_string(this->y) +
		";" + std::to_string(this->z) + ";" + std::to_string(this->w) + ")";
}

float float4::length()
{
	return sqrtf(powf(this->x, 2) + powf(this->y, 2) + powf(this->z, 2) + powf(this->w, 2));
}

float float4::lengthSquared()
{
	return (powf(this->x, 2) + powf(this->y, 2) + powf(this->z, 2) + powf(this->w, 2));
}

float float4::dot(float4 v)
{
	return this->x * v.GetX() +
		this->y * v.GetY() +
		this->z * v.GetZ() +
		this->w * v.GetW();
}

void float4::negate()
{
	this->x = -1 * this->x;
	this->y = -1 * this->y;
	this->z = -1 * this->z;
	this->w = -1 * this->w;
}

void float4::add(float4 v)
{
	this->x += v.GetX();
	this->y += v.GetY();
	this->z += v.GetZ();
	this->w += v.GetW();

}

void float4::sub(float4 v)
{
	this->x -= v.GetX();
	this->y -= v.GetY();
	this->z -= v.GetZ();
	this->w -= v.GetW();
}

void float4::div(float f)
{
	if (f != 0)
	{
		this->x = this->x / f;
		this->y = this->y / f;
		this->z = this->z / f;
		this->w = this->w / f;
	}
	else
	{
		throw std::overflow_error("Cant divide by 0");

	}
}

void float4::mag(float f)
{
	this->x = this->x * f;
	this->y = this->y * f;
	this->z = this->z * f;
	this->w = this->w * f;
}

void float4::normalize()
{
	double length = this->length();
	if (length > epsilon)
	{
		this->div(length);
	}
	else
	{
		x = 0;
		y = 0;
		z = 0;
		w = 0;
	}
}

float4 float4::normalizeProduct()
{
	float4 newVector = float4(this->x, this->y, this->z, this->w);
	double length = newVector.length();
	if( length > epsilon)
	{
		newVector.div(length);
		return newVector;
	}
	else
	{
		return newVector; // throw new Exception("Couldn't normalize");
	}

}

float4 float4::reflect(float4 normal)
{
	//return this - 2*(this.dot(normal) * normal);
	double temp = 2 * this->dot(normal);
	return float4(this->x - temp * normal.x, this->y - temp * normal.y,
		this->z - temp * normal.z, this->w - temp * normal.w);

}

float4 float4::magProduct(float4 v, double f)
{
	return float4(v.GetX() * f, v.GetY() *f, v.GetZ() * f, v.GetW() * f);
}

float4 float4::lerp(float4 v, double t)
{
	float4 newVector4 = float4();
	newVector4.SetX(this->x + t * (v.GetX() - this->x));
	newVector4.SetY(this->y + t * (v.GetY() - this->y));
	newVector4.SetZ(this->z + t * (v.GetZ() - this->z));
	newVector4.SetW(this->w + t * (v.GetW() - this->w));

	return newVector4;

}

float4 float4::toPoint()
{
	return float4(this->x, this->y, this->z, this->w);
}

float4 float4::operator*(double scalar)
{
	return float4(this->x = scalar,
		this->y = scalar,
		this->z = scalar,
		this->w = scalar);
}

float4 float4::operator*(float4 right)
{
	return float4(this->x * right.GetX(),
		this->y * right.GetY(),
		this->z * right.GetZ(),
		this->w * right.GetW());
}



float4 float4::operator+(float4 right)
{
	return float4(this->x + right.GetX(),
		this->y + right.GetY(),
		this->z + right.GetZ(),
		this->w + right.GetW()
	);
}



float4 float4::operator-(float4 right)
{
	return float4(this->x - right.GetX(),
		this->y - right.GetY(),
		this->z - right.GetZ(),
		this->w - right.GetW()
	);
}

bool float4::operator==(float4 right)
{
	if (this->x == right.GetX() && this->y == right.GetY()
		&& this->z == right.GetZ() && this->w != right.GetW())
	{
		return true;
	}
	return false;
}

bool float4::operator!=(float4 right)
{
	return (this->x != right.GetX() || this->y != right.GetY()
		|| this->z != right.GetZ() || this->w != right.GetW());
}

float4 float4::operator/(float scalar)
{
	float4 vector = float4();
	// get the inverse of the scalar up front to avoid doing multiple divides later
	double inverse = 1.0f / scalar;
	vector.SetX(this->x * inverse);
	vector.SetY(this->y * inverse);
	vector.SetZ(this->z * inverse);
	vector.SetW(this->w * inverse);
	return vector;
}

float4 float4::Reflect(float4 normal)
{
	double dot = normal.dot(*this);
	return normal * dot * 2 - *this;
}

float4 float4::Reflect(float4 vec, float4 normal)
{

	double dot = normal.dot(vec);
	return normal * dot * 2 - vec;

}

float4& float4::operator=(const float4 &vector)
{
	x = vector.x;
	y = vector.y;
	z = vector.z;
	w = vector.w;

	return *this;
}

