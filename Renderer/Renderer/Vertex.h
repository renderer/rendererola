#pragma once
#include "float3.h"
#include "RGB.h"
class Vertex
{

public:
	Vertex();
	Vertex(float3 position);
	Vertex(float3 position, float3 normal);
	Vertex(float3 position, float3 normal, RGB color);
	Vertex(float3 position, float3 normal, RGB color, float3 UV);
	~Vertex();;

	float3 position;
	float3 normal;
	RGB color;
	float3 UV;

	std::string ToString();
	
};

