#pragma once
#include "pch.h"
#include <string>
#include <algorithm>

class  float3
{
	const float epsilon = 0.000001f;
public:
	float x, y, z;

	float3(float x, float y, float z);
	float3();
	float3(float3 v1, float3 v2);
	~float3();

	void setX(float x);
	void setY(float y);
	void setZ(float z);
	float getX();
	float getY();
	float getZ();

	std::string ToString();

	float length();
	float lengthSquared();
	float dot(float3 v);
	float3 cross(float3 v);
	void negate();
	void add(float3 v);
	void sub(float3 v);
	void div(float f);
	void mag(float f);
	void normalize();
	float3 normalizeProduct();
	float3 reflect(float3 normal);
	float3 magProduct(float3 v, double f);
	float3 lerp(float3 v, double t);
	float3 toPoint();
	float3 operator*(double scalar);
	float3 operator*(float3 right);
	float3 operator+(float3 right);
	float3 operator-(float3 right);
	bool operator==(float3 right);
	bool operator!=(float3 right);
	float3 operator/(float scalar);
	float3 Reflect(float3 normal);
	float3 Reflect(float3 vec, float3 normal);
	float3& operator=(const float3 &rhs);


};

float clamp(const float & value, const float & minValue, const float & maxValue);
float clamp01(const float & value);

//
//double clamp(const double & value, const double & minValue, const double & maxValue)
//{
//	return std::max(minValue, std::min(value, maxValue));
//}
//double clamp01(const double & value)
//{
//	return clamp(value, 0.0F, 1.0F);
//}
//
////Refracts given vector hitting in the surface with given normal
////taking into consideration fromEnvironmentRefractiveFactor from which vector came and toEnvironmentRefractiveFactor to which vector will go after refraction
//float3 refract(float3 inVectorDirection, float3 normalSurface, const double& fromEnvironmentRefractiveFactor, const double& toEnvironmentRefractiveFactor)
//{
//	float3 minuend;
//	float3 subtrahend;
//
//	double inVectorDirectionDotNormalSurface = inVectorDirection.dot(normalSurface);
//
//	minuend = ((inVectorDirection - (normalSurface * (inVectorDirectionDotNormalSurface)) * fromEnvironmentRefractiveFactor)) / toEnvironmentRefractiveFactor;
//
//	double underRootValue = 1.0F - (((fromEnvironmentRefractiveFactor * fromEnvironmentRefractiveFactor) * (1.0F - (inVectorDirectionDotNormalSurface * inVectorDirectionDotNormalSurface))) / (toEnvironmentRefractiveFactor * toEnvironmentRefractiveFactor));
//
//	subtrahend = normalSurface * sqrtf(underRootValue);
//
//	float3 finalVector = minuend - subtrahend;
//
//	return finalVector;
//}
//
////Checks if given vector values equal nan
//bool isVectorNaN(float3& vector)
//{
//	return isnan(vector.getX()) || isnan(vector.getY()) || isnan(vector.getZ());
//
//}