#pragma once
#include "float4.h"
#include <string>
class float4;


class float4x4
{
public:
	float4 column[4];

	float4x4();
	float4x4(float4 a, float4 b, float4 c, float4 d);
	~float4x4();

	float4x4 mul(float4x4 b);
	float4x4 transpone();
	std::string toString();

};

