#pragma once
#include "Buffer.h"
#include "float3.h"
#include "Vertex.h"
#include"Light.h"

class Rasterizer
{
public:
	Rasterizer(Buffer &b);
	~Rasterizer();

	void triangle(Vertex v1, Vertex v2, Vertex v3);
	void triangle2(Vertex v1, Vertex v2, Vertex v3, VertexProcessor & vp, Light & light, Texture *texture = nullptr);
	void triangle3(Vertex v1, Vertex v2, Vertex v3, VertexProcessor & vp, std::vector<Light*> lights, Texture *texture = nullptr);
	/*void triangle(const float3 v1, const float3 v2, const float3 v3,
		RGB rgb1 = RGB(255, 0, 0), RGB rgb2 = RGB(0, 255, 0), RGB rgb3 = RGB(0, 0, 255));*/

protected:
	Buffer &buffer;
};

