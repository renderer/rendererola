#pragma once
#include "pch.h"
class Ppm
{
private:
	int width;
	int height;
	

public:
	Ppm();
	Ppm(const std::string& name);
	Ppm(const std::string& name, int i);
	Ppm(int xsize, int ysize);

	int getWidth();
	int getHeight();

	virtual ~Ppm();

private:

};
