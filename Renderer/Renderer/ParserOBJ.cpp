#include "pch.h"
#include "ParserOBJ.h"


ParserOBJ::ParserOBJ()
{
}


ParserOBJ::~ParserOBJ()
{
}



std::vector<Mesh> ParserOBJ::LoadMesh(const std::string & fileName)
{
	std::vector<float3> points;
	std::vector<float3> normals;
	std::vector<float3> textures;


	std::ifstream objFile;
	objFile.open(fileName);
	std::string line;

	std::vector<Mesh> meshes;

	while (std::getline(objFile, line))
	{
		if (line.size() == 0)
		{
			continue;
		}

		if (line[0] == 'v')
		{
			//tu plik nie ma nazwy, ale dalej ma zmienne 
			//wspo�rz�dne 
			//wektory noramlne
			//teksturowania

			parseV(line, points, normals, textures);
		}
		if (line[0] == 'g')
		{
			//mamy nowy obiekt
			meshes.push_back(Mesh());
			//wpisz t� pi�kn� nazw�
			meshes.at(meshes.size() - 1).name = PardeG(line);

			do
			{
				std::getline(objFile, line);
				if (line[0] == 'f')
				{
					Triangle triangle = Triangle();
					triangle = parseF(line, points, normals, textures);

					meshes.at(meshes.size() - 1).meshTriangles.push_back(triangle);
				}

			} while (line[0] != '#');

			//i �ciany

		}

	}


	objFile.close();

	return meshes;

}

std::vector<std::string> ParserOBJ::splitString(std::string &line, std::string delim)
{

	std::vector<std::string> tokens;
	size_t prev = 0, pos = 0;
	do
	{
		pos = line.find(delim, prev);
		if (pos == std::string::npos) pos = line.length();
		std::string token = line.substr(prev, pos - prev);
		if (!token.empty()) tokens.push_back(token);
		prev = pos + delim.length();
	} while (pos < line.length() && prev < line.length());
	return tokens;
}

void ParserOBJ::parsefloat3(std::string & line, std::vector<float3>& vectorCollection)
{
	std::vector<std::string> splitLine = splitString(line, " ");
	if (splitLine.size() != 4)
	{
		throw "Wrong float3 definition in .obj file.";
	}
	float x = (float)stod(splitLine[1]);
	float y = (float)stod(splitLine[2]);
	float z = (float)stod(splitLine[3]);
	vectorCollection.push_back(float3(x, y, z));
}

void ParserOBJ::parseV(std::string & line, std::vector<float3>& points, std::vector<float3>& normals, std::vector<float3>& textures)
{
	switch (line[1])
	{
	case ' ':
		parsefloat3(line, points);
		break;
	case 'n':
		parsefloat3(line, normals);
		break;
	case 't':
		parsefloat3(line, textures);
		break;
	default:
		throw "Unknown data in .obj file.";
	}
}


Triangle ParserOBJ::parseF(std::string line, std::vector<float3>& points, std::vector<float3>& normals, std::vector<float3>& textures)
{
	std::vector<std::string> splitOne = splitString(line, " ");

	std::vector<float3> cordinates;
	std::vector<float3> normalsCor;
	std::vector<float3> texturesCor;

	std::vector<std::string>::iterator iterator;
	for (iterator = splitOne.begin() + 1; iterator != splitOne.end(); ++iterator)
	{
		std::vector<std::string> splitTwo = splitString(*iterator, "/");

		if (splitTwo.size() == 3)
		{
			if (splitTwo[0].size() > 0)
			{
				//mamy numer punktu
				cordinates.push_back(points.at(std::stoi(splitTwo[0]) - 1));
			}

			if (splitTwo[1].size() > 0)
			{
				//mamy numer punktu
				texturesCor.push_back(textures.at(std::stoi(splitTwo[1]) - 1));
			}
			if (splitTwo[2].size() > 0)
			{
				//mamy numer punktu
				normalsCor.push_back(normals.at(std::stoi(splitTwo[2]) - 1));
			}
		}
		if (splitTwo.size() == 2)
		{
			if ((*iterator).find("//") != std::string::npos)
			{

				if (splitTwo[0].size() > 0)
				{
					//mamy numer punktu
					cordinates.push_back(points.at(std::stoi(splitTwo[0]) - 1));
				}
				if (splitTwo[1].size() > 0)
				{
					//mamy numer punktu
					normalsCor.push_back(normals.at(std::stoi(splitTwo[1]) - 1));
				}
				/*if (splitTwo[2].size() > 0)*/
				{
					//mamy numer punktu
					texturesCor.push_back(float3());
				}
			}
			else
			{

				if (splitTwo[0].size() > 0)
				{
					//mamy numer punktu
					cordinates.push_back(points.at(std::stoi(splitTwo[0]) - 1));
				}
				//if (splitTwo[1].size() > 0)
				{
					//mamy numer punktu
					normalsCor.push_back(float3());
				}
				if (splitTwo[1].size() > 0)
				{
					//mamy numer punktu
					texturesCor.push_back(textures.at(std::stoi(splitTwo[1]) - 1));
				}
			}
		}
		if (splitTwo.size() == 1)
		{
			if (splitTwo[0].size() > 0)
			{
				//mamy numer punktu
				cordinates.push_back(points.at(std::stoi(splitTwo[0]) - 1));
			}
			if (splitTwo[1].size() > 0)
			{
				//mamy numer punktu
				normalsCor.push_back(float3());
			}

			if (splitTwo[2].size() > 0)
			{
				//mamy numer punktu
				texturesCor.push_back(float3());
			}
		}
	}

	//mmay dane


	return Triangle(cordinates[0], cordinates[1], cordinates[2], normalsCor[0], normalsCor[1], normalsCor[2],
		texturesCor[0], texturesCor[1], texturesCor[2]);


}

std::string ParserOBJ::PardeG(std::string & line)
{
	std::vector<std::string> split = splitString(line, " ");
	if (split.size() < 2)
	{
		std::cout << "Zly format. brak nazwy w pliku OBJ" << std::endl;
		return "";
	}
	else
	{
		return split[1];
	}

}



