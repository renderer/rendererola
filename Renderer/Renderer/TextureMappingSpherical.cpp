#include "pch.h"

#include "TextureMappingSpherical.h"


TextureMappingSpherical::TextureMappingSpherical(float scale) : TextureMapping(scale)
{
}


TextureMappingSpherical::~TextureMappingSpherical()
{
}

void TextureMappingSpherical::getPixelCoordinates(float3 & localHitPoint, const unsigned int width, const unsigned int height, int & row, int & column)
{
	float phi = calculatePhi(localHitPoint);
	float theta = acosf(-localHitPoint.getY());

	float u = phi * (1.0F / (2.0F * (float)M_PI));
	float v = 1.0F - theta * ((float)M_1_PI);

	getRowAndColumn(u, v, width, height, row, column);
}
