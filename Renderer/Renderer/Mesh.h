#pragma once
#include <vector>
#include <string>
#include "float3.h"
#include "Triangle.h"
#include <fstream>
#include "Light.h"

class Mesh
{
public:


	std::string name;					//g
	std::vector<float3> points;		    //v
	std::vector<float3> normals;		//vn
	std::vector<float3> textures;		//vt
	std::vector<float3> connections;	//f
	std::vector<Triangle> meshTriangles;


	Mesh();
	~Mesh();

	void Draw(Rasterizer& r, VertexProcessor& vp, Light & light);
	void Draw2(Rasterizer& r, VertexProcessor& vp, Light & light);
	void Draw3(Rasterizer& r, VertexProcessor& vp, std::vector<Light*> lights);
	void Draw(Rasterizer & r, VertexProcessor & vp);

	
};

