#include "pch.h"
#include "Rasterizer.h"
#include <algorithm>


Rasterizer::Rasterizer(Buffer &b) : buffer(b)
{

}


Rasterizer::~Rasterizer()
{
}

inline float minMy(float a, float b, float c)
{
	return a < b ? (a < c ? a : c) : (b < c ? b : c);
}

inline float maxMy(float a, float b, float c)
{
	return a > b ? (a > c ? a : c) : (b > c ? b : c);
}

void Rasterizer::triangle(Vertex v1, Vertex v2, Vertex v3)
{
	float x1 = (v1.position.x + 1) * buffer.GetWidth() * 0.5f;
	float x2 = (v2.position.x + 1) * buffer.GetWidth() * 0.5f;
	float x3 = (v3.position.x + 1) * buffer.GetWidth() * 0.5f;

	float y1 = (v1.position.y + 1) * buffer.GetHeight() * 0.5f;
	float y2 = (v2.position.y + 1) * buffer.GetHeight() * 0.5f;
	float y3 = (v3.position.y + 1) * buffer.GetHeight() * 0.5f;

	float xmin = minMy(x1, x2, x3);
	float xmax = maxMy(x1, x2, x3);
	float ymin = minMy(y1, y2, y3);
	float ymax = maxMy(y1, y2, y3);

	xmin = std::max(xmin, 0.0f);
	xmax = std::min(xmax, float(buffer.GetWidth() - 1));
	ymin = std::max(ymin, 0.0f);
	ymax = std::min(ymax, float(buffer.GetHeight() - 1));


	float dx12 = x1 - x2;
	float dx23 = x2 - x3;
	float dx31 = x3 - x1;
	float dy12 = y1 - y2;
	float dy23 = y2 - y3;
	float dy31 = y3 - y1;

	float db1 = 1.f / (-dy23 * dx31 + dx23 * dy31);
	float db2 = 1.f / (dy31 * dx23 - dx31 * dy23);

	bool tl1 = false;
	bool tl2 = false;
	bool tl3 = false;

	if (dy12 < 0 || (dy12 == 0 && dx12 > 0)) { tl1 = true; }
	if (dy23 < 0 || (dy23 == 0 && dx23 > 0)) { tl2 = true; }
	if (dy31 < 0 || (dy31 == 0 && dx31 > 0)) { tl3 = true; }

	for (int x = xmin; x <= xmax; x++)
	{
		for (int y = ymin; y <= ymax; y++)
		{
			if (((dx12*(y - y1) - dy12 * (x - x1) > 0 && !tl1) || (dx12 * (y - y1) - dy12 * (x - x1) >= 0) && tl1) &&
				((dx23*(y - y2) - dy23 * (x - x2) > 0 && !tl2) || (dx23 * (y - y2) - dy23 * (x - x2) >= 0) && tl2) &&
				((dx31*(y - y3) - dy31 * (x - x3) > 0 && !tl3) || (dx31 * (y - y3) - dy31 * (x - x3) >= 0) && tl3))
			{
				//barycentryczne
				float
					lambda1 = (dy23 * (x - x3) - dx23 * (y - y3)) * db1,
					lambda2 = (dy31 * (x - x3) - dx31 * (y - y3)) * db2,
					lambda3 = 1 - lambda1 - lambda2;

				float depth = lambda1 * v1.position.z + lambda2 * v2.position.z + lambda3 * v3.position.z;
				if (depth > buffer.GetDepth(x, y))
				{
					RGB newRGB = v1.color * lambda1 + v2.color * lambda2 + v3.color * lambda3;

					buffer.SetColorInPixel(x, y, newRGB);
					buffer.SetDepth(x, y, depth);
				}


			}
		}
	}


}


void Rasterizer::triangle2(Vertex v1, Vertex v2, Vertex v3, VertexProcessor & vp, Light & light, Texture *texture)
{
	float x1 = (v1.position.x + 1) * buffer.GetWidth() * 0.5f;
	float x2 = (v2.position.x + 1) * buffer.GetWidth() * 0.5f;
	float x3 = (v3.position.x + 1) * buffer.GetWidth() * 0.5f;

	float y1 = (v1.position.y + 1) * buffer.GetHeight() * 0.5f;
	float y2 = (v2.position.y + 1) * buffer.GetHeight() * 0.5f;
	float y3 = (v3.position.y + 1) * buffer.GetHeight() * 0.5f;

	float xmin = minMy(x1, x2, x3);
	float xmax = maxMy(x1, x2, x3);
	float ymin = minMy(y1, y2, y3);
	float ymax = maxMy(y1, y2, y3);

	xmin = std::max(xmin, 0.0f);
	xmax = std::min(xmax, float(buffer.GetWidth() - 1));
	ymin = std::max(ymin, 0.0f);
	ymax = std::min(ymax, float(buffer.GetHeight() - 1));


	float dx12 = x1 - x2;
	float dx23 = x2 - x3;
	float dx31 = x3 - x1;
	float dy12 = y1 - y2;
	float dy23 = y2 - y3;
	float dy31 = y3 - y1;

	float db1 = 1.f / (-dy23 * dx31 + dx23 * dy31);
	float db2 = 1.f / (dy31 * dx23 - dx31 * dy23);

	bool tl1 = false;
	bool tl2 = false;
	bool tl3 = false;

	if (dy12 < 0 || (dy12 == 0 && dx12 > 0)) { tl1 = true; }
	if (dy23 < 0 || (dy23 == 0 && dx23 > 0)) { tl2 = true; }
	if (dy31 < 0 || (dy31 == 0 && dx31 > 0)) { tl3 = true; }

	for (int x = xmin; x <= xmax; x++)
	{
		for (int y = ymin; y <= ymax; y++)
		{
			if (((dx12*(y - y1) - dy12 * (x - x1) > 0 && !tl1) || (dx12 * (y - y1) - dy12 * (x - x1) >= 0) && tl1) &&
				((dx23*(y - y2) - dy23 * (x - x2) > 0 && !tl2) || (dx23 * (y - y2) - dy23 * (x - x2) >= 0) && tl2) &&
				((dx31*(y - y3) - dy31 * (x - x3) > 0 && !tl3) || (dx31 * (y - y3) - dy31 * (x - x3) >= 0) && tl3))
			{
				//barycentryczne
				float
					lambda1 = (dy23 * (x - x3) - dx23 * (y - y3)) * db1,
					lambda2 = (dy31 * (x - x3) - dx31 * (y - y3)) * db2,
					lambda3 = 1 - lambda1 - lambda2;

				Vertex newVertex =
					Vertex(float3(v1.position*lambda1 + v2.position *lambda2 + v3.position*lambda3),
						float3(v1.normal*lambda1 + v2.normal *lambda2 + v3.normal*lambda3),
						v1.color,
						float3(v1.UV *lambda1 + v2.UV *lambda2 + v3.UV*lambda3));

				RGB color = light.calcuate({ newVertex.position,newVertex.normal, RGB(), newVertex.UV }, vp, texture);

				float depth = lambda1 * v1.position.z + lambda2 * v2.position.z + lambda3 * v3.position.z;
				if (depth > buffer.GetDepth(x, y))
				{
					RGB newRGB = color;
					/*RGB newRGB = v1.color * lambda1 + v2.color * lambda2 + v3.color * lambda3;

					if (texture != nullptr)
					{
						RGB newRGB = v1.color * lambda1 + v2.color * lambda2 + v3.color * lambda3;

					}*/



					buffer.SetColorInPixel(x, y, newRGB);
					buffer.SetDepth(x, y, depth);
				}


			}
		}
	}


}

void Rasterizer::triangle3(Vertex v1, Vertex v2, Vertex v3, VertexProcessor & vp, std::vector<Light*> lights, Texture *texture)
{
	float x1 = (v1.position.x + 1) * buffer.GetWidth() * 0.5f;
	float x2 = (v2.position.x + 1) * buffer.GetWidth() * 0.5f;
	float x3 = (v3.position.x + 1) * buffer.GetWidth() * 0.5f;

	float y1 = (v1.position.y + 1) * buffer.GetHeight() * 0.5f;
	float y2 = (v2.position.y + 1) * buffer.GetHeight() * 0.5f;
	float y3 = (v3.position.y + 1) * buffer.GetHeight() * 0.5f;

	float xmin = minMy(x1, x2, x3);
	float xmax = maxMy(x1, x2, x3);
	float ymin = minMy(y1, y2, y3);
	float ymax = maxMy(y1, y2, y3);

	xmin = std::max(xmin, 0.0f);
	xmax = std::min(xmax, float(buffer.GetWidth() - 1));
	ymin = std::max(ymin, 0.0f);
	ymax = std::min(ymax, float(buffer.GetHeight() - 1));


	float dx12 = x1 - x2;
	float dx23 = x2 - x3;
	float dx31 = x3 - x1;
	float dy12 = y1 - y2;
	float dy23 = y2 - y3;
	float dy31 = y3 - y1;

	float db1 = 1.f / (-dy23 * dx31 + dx23 * dy31);
	float db2 = 1.f / (dy31 * dx23 - dx31 * dy23);

	bool tl1 = false;
	bool tl2 = false;
	bool tl3 = false;

	if (dy12 < 0 || (dy12 == 0 && dx12 > 0)) { tl1 = true; }
	if (dy23 < 0 || (dy23 == 0 && dx23 > 0)) { tl2 = true; }
	if (dy31 < 0 || (dy31 == 0 && dx31 > 0)) { tl3 = true; }

	for (int x = xmin; x <= xmax; x++)
	{
		for (int y = ymin; y <= ymax; y++)
		{
			if (((dx12*(y - y1) - dy12 * (x - x1) > 0 && !tl1) || (dx12 * (y - y1) - dy12 * (x - x1) >= 0) && tl1) &&
				((dx23*(y - y2) - dy23 * (x - x2) > 0 && !tl2) || (dx23 * (y - y2) - dy23 * (x - x2) >= 0) && tl2) &&
				((dx31*(y - y3) - dy31 * (x - x3) > 0 && !tl3) || (dx31 * (y - y3) - dy31 * (x - x3) >= 0) && tl3))
			{
				//barycentryczne
				float
					lambda1 = (dy23 * (x - x3) - dx23 * (y - y3)) * db1,
					lambda2 = (dy31 * (x - x3) - dx31 * (y - y3)) * db2,
					lambda3 = 1 - lambda1 - lambda2;

				Vertex newVertex =
					Vertex(float3(v1.position*lambda1 + v2.position *lambda2 + v3.position*lambda3),
						float3(v1.normal*lambda1 + v2.normal *lambda2 + v3.normal*lambda3),
						v1.color,
						float3(v1.UV *lambda1 + v2.UV *lambda2 + v3.UV*lambda3));


				//�wiat�o
				RGB color = RGB(1, 1, 1);
				RGB specularColor = RGB(1, 1, 1);
				RGB newtextureColor = RGB(1, 1, 1);
				float kspot = 1;
				std::vector<Light*>::iterator iterator2;
				for (iterator2 = lights.begin(); iterator2 != lights.end(); ++iterator2)
				{
					color += (*iterator2)->calcuateOnlyColor({ newVertex.position,newVertex.normal, RGB(), newVertex.UV }, vp);
					specularColor += (*iterator2)->calcuateOnlyColorSpecalur({ newVertex.position,newVertex.normal, RGB(), newVertex.UV }, vp);
					float aaa = (*iterator2)->calcuateOnlyColorSpotLight({ newVertex.position,newVertex.normal, RGB(), newVertex.UV }, vp);
					if (aaa < 1.0f)
					{
						kspot = aaa;
					}
				}

				if (texture != nullptr)
				{
					newtextureColor = texture->getColor(newVertex.UV.getX(), newVertex.UV.getY(), newVertex.position);
				}

				color = color * newtextureColor + specularColor;
				color = color * kspot;

				//koniec swiat�a	

				float depth = lambda1 * v1.position.z + lambda2 * v2.position.z + lambda3 * v3.position.z;
				if (depth > buffer.GetDepth(x, y))
				{
					RGB newRGB = color;
					/*RGB newRGB = v1.color * lambda1 + v2.color * lambda2 + v3.color * lambda3;

					if (texture != nullptr)
					{
						RGB newRGB = v1.color * lambda1 + v2.color * lambda2 + v3.color * lambda3;

					}*/



					buffer.SetColorInPixel(x, y, newRGB);
					buffer.SetDepth(x, y, depth);
				}


			}
		}
	}


}