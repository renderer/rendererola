#include "pch.h"
#include "Texture.h"


Texture::Texture()
{
}

Texture::Texture(const std::string & fileName)
{
	image = TgaBuffer(fileName);
	textureMapping = nullptr;
}

Texture::Texture(const std::string & fileName, TextureMappingTypes textureMappingType, float textureScale)
{

	switch (textureMappingType)
	{
	case PlanarAKARectangularWithXAllignment:
		textureMapping = new TextureMappingRectangular(textureScale, Allignment::X);
		break;
	case PlanarAKARectangularWithYAllignment:
		textureMapping = new TextureMappingRectangular(textureScale, Allignment::Y);
		break;
	case PlanarAKARectangularWithZAllignment:
		textureMapping = new TextureMappingRectangular(textureScale, Allignment::Z);
		break;
	case Spherical:
		textureMapping = new TextureMappingSpherical(textureScale);
		break;
	}

	image = TgaBuffer(fileName);

}

RGB Texture::getColor(const float & u, const float & v, float3 hit)
{
	if (image.GetHeight() == 0 & image.GetHeight() == 0)
	{
		return RGB(1, 1, 1);
	}

	int row;
	int column;

	if (textureMapping != nullptr)
	{
		textureMapping->getPixelCoordinates(hit, image.GetWidth(), image.GetHeight(), row, column);
			//getRowAndColumn(u, v, image.GetWidth(), image.GetHeight(), row, column);
	}
	else
	{
		getRowAndColumn(u, v, image.GetWidth(), image.GetHeight(), row, column);// czy 1-u
	}

	return image.GetColorInPixel(row, column);
}

void Texture::getRowAndColumn(const float & u, const float & v, const unsigned int & width, const unsigned int & height, int & row, int & column)
{
	row = (int)((width - 1) * u);
	column = (int)((height - 1) * v);
}


Texture::~Texture()
{
}
