#pragma once
#include "float3.h"
#include "RGB.h"
#include "VertexProcessor.h"
#include "Vertex.h"
#include "Texture.h"

 class  Light
{
protected:
	float3 position;
	RGB ambient;
	RGB diffuse;
	RGB specular;
	Texture *texture;

public:
	Light(float3 position, RGB ambient, RGB diffuse, RGB specular );
	~Light();

	virtual RGB calcuate(Vertex vertex, VertexProcessor& vp, Texture * texture = nullptr) = 0;
	virtual RGB calcuateOnlyColor(Vertex vertex, VertexProcessor & vp) =0;
	virtual RGB calcuateOnlyColorSpecalur(Vertex vertex, VertexProcessor & vp) =0;
	virtual float calcuateOnlyColorSpotLight(Vertex vertex, VertexProcessor & vp) =0;


};

