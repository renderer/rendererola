#include <vector>
#include "RGB.h"
#pragma once
class Buffer
{
private:
	int width;
	int height;
	int length;
	void setBackground(RGB color = RGB(0, 0, 0));
	void setDepth(float value = -1.0f);

public:
	std::vector<RGB> VectorsColor;
	std::vector<float> Depth;
	Buffer();
	Buffer(int width, int height);
	Buffer(int width, int height, RGB color);
	~Buffer();

	int GetWidth();
	void SetWidth(int width);
	int GetHeight();
	void SetHeight(int height);
	int GetLength();
	void CalculateLength();
	void SetSize();
	void SetColorInPixel(int i, int j, RGB color);
	void SetDepth(int i, int j, float value);
	RGB GetColorInPixel(int i, int j);
	float GetDepth(int i, int j);
};

