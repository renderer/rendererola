#include "pch.h"
#include "float3.h"

float3::~float3()
{
}

float3::float3(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}


float3::float3()
{
	this->x = 0.0f;
	this->y = 0.0f;
	this->z = 0.0f;
}

float3::float3(float3 v1, float3 v2)
{
	this->x = v2.getX() - v1.getX();
	this->y = v2.getY() - v1.getY();
	this->z = v2.getZ() - v1.getZ();
}

void float3::setX(float x)
{
	this->x = x;
}

void float3::setY(float y)
{
	this->y = y;
}

void float3::setZ(float z)
{
	this->z = z;
}

float float3::getX()
{
	return this->x;
}

float float3::getY()
{
	return this->y;
}

float float3::getZ()
{
	return this->z;
}

std::string float3::ToString()
{
	return "float3(" + std::to_string(this->x) + ";" + std::to_string(this->y) + ";" + std::to_string(this->z) + ")";
}

float float3::length()
{
	return sqrt (powf(this->x, 2) + powf(this->y, 2) + powf(this->z, 2));
}

float float3::lengthSquared()
{
	return powf(this->x, 2) + powf(this->y, 2) + powf(this->z, 2);
}

float float3::dot(float3 v)
{
	return this->x * v.getX() +
		this->y * v.getY() +
		this->z * v.getZ();
}

float3 float3::cross(float3 v)
{
	return float3(this->y * v.getZ() - this->z * v.getY(),
		this->z * v.getX() - this->x * v.getZ(),
		this->x * v.getY() - this->y * v.getX());
}

void float3::negate()
{
	this->x = -1 * this->x;
	this->y = -1 * this->y;
	this->z = -1 * this->z;
}

void float3::add(float3 v)
{
	this->x += v.getX();
	this->y += v.getY();
	this->z += v.getZ();

}

void float3::sub(float3 v)
{
	this->x -= v.getX();
	this->y -= v.getY();
	this->z -= v.getZ();
}

void float3::div(float f)
{
	if (f != 0)
	{
		this->x = this->x / f;
		this->y = this->y / f;
		this->z = this->z / f;
	}
	else
	{
		throw std::overflow_error("Cant divide by 0");

	}
}

void float3::mag(float f)
{
	this->x = this->x * f;
	this->y = this->y * f;
	this->z = this->z * f;
}

void float3::normalize()
{
	double length = this->length();
	if (length > epsilon)
	{
		this->div(length);
	}
	else
	{
		x = 0;
		y = 0;
		z = 0;
	}
}

float3 float3::normalizeProduct()
{
	float3 newVector = float3(this->x, this->y, this->z);
	double length = newVector.length();
	if (length > epsilon)
	{
		newVector.div(length);
		return newVector;
	}
	else
	{
		return newVector; // throw new Exception("Couldn't normalize");
	}

}

float3 float3::reflect(float3 normal)
{
	//return this - 2*(this.dot(normal) * normal);
	double temp = 2 * this->dot(normal);
	return float3(this->x - temp * normal.x, this->y - temp * normal.y, this->z - temp * normal.z);

}

float3 float3::magProduct(float3 v, double f)
{
	return float3(v.getX() * f, v.getY() *f, v.getZ() * f);
}

float3 float3::lerp(float3 v, double t)
{
	float3 newfloat3 = float3();
	newfloat3.setX(this->x + t * (v.getX() - this->x));
	newfloat3.setY(this->y + t * (v.getY() - this->y));
	newfloat3.setZ(this->z + t * (v.getZ() - this->z));

	return newfloat3;

}

float3 float3::toPoint()
{
	return float3(this->x, this->y, this->z);
}

float3 float3::operator*(double scalar)
{
	return float3(this->x * scalar,
		this->y * scalar,
		this->z * scalar);
}

float3 float3::operator*(float3 right)
{
	return float3(this->x * right.getX(),
		this->y * right.getY(),
		this->z * right.getZ());
}



float3 float3::operator+(float3 right)
{
	return float3(this->x + right.getX(),
		this->y + right.getY(),
		this->z + right.getZ()
	);
}



float3 float3::operator-(float3 right)
{
	return float3(this->x - right.getX(),
		this->y - right.getY(),
		this->z - right.getZ()
	);
}

bool float3::operator==(float3 right)
{
	if (this->x == right.getX() && this->y == right.getY() && this->z == right.getZ())
	{
		return true;
	}
	return false;
}

bool float3::operator!=(float3 right)
{
	return (this->x != right.getX() || this->y != right.getY() || this->z != right.getZ());
}

float3 float3::operator/(float scalar)
{
	float3 vector = float3();
	// get the inverse of the scalar up front to avoid doing multiple divides later
	double inverse = 1.0f / scalar;
	vector.setX(this->x * inverse);
	vector.setY(this->y * inverse);
	vector.setZ(this->z * inverse);
	return vector;
}

float3 float3::Reflect(float3 normal)
{
	double dot = normal.dot(*this);
	return normal * dot * 2 - *this;
}

float3 float3::Reflect(float3 vec, float3 normal)
{

	double dot = normal.dot(vec);
	return normal * dot * 2 - vec;

}

float3& float3::operator=(const float3 &rhs)
{
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;

	return *this;
}


float clamp(const float & value, const float & minValue, const float & maxValue)
{
	return std::max(minValue, std::min(value, maxValue));
}

float clamp01(const float & value)
{
	return clamp(value, 0.0F, 1.0F);
}