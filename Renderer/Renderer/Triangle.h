#pragma once
#include "float3.h"
#include "Rasterizer.h"
#include "VertexProcessor.h"
#include "Light.h"
#include "Texture.h"
class Triangle
{
private:

	Vertex va;
	Vertex vb;
	Vertex vc;
	
	float3 normalVector;
	

public:
	Texture* texture;
	Triangle();

	Triangle(float3 a, float3 b, float3 c, float3 normal);
	Triangle(float3 a, float3 b, float3 c);

	Triangle(float3 a, float3 b, float3 c, float3 Na, float3 Nb, float3 Nc, float3 Ua, float3 Ub, float3 Uc);
	
	Triangle(float3 a, float3 b, float3 c, float3 Na, float3 Nb, float3 Nc, float3 Ua, float3 Ub, float3 Uc, Texture* texture);

	~Triangle();

	void Draw(Rasterizer& r, VertexProcessor& vp, Light & light);
	void Draw2(Rasterizer& r, VertexProcessor& vp, Light & light);
	void Draw3(Rasterizer & r, VertexProcessor & vp, std::vector<Light*> lights);
	void Draw(Rasterizer & r, VertexProcessor & vp);
	
	float CalculateSurfaceArea();
};

