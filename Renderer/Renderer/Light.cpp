#include "pch.h"
#include "Light.h"


Light::Light(float3 position, RGB ambient, RGB diffuse, RGB specular)
{
	this->position = position;
	this->ambient = ambient;
	this->diffuse = diffuse;
	this->specular = specular;

}

Light::~Light()
{
}
