#include "pch.h"
#include "Buffer.h"


void Buffer::setBackground(RGB color)
{
	for (int i = 0; i < length; i++)
	{
			VectorsColor[i] = color;
	}
}

void Buffer::setDepth(float value)
{
	for (int i = 0; i < length; i++)
	{
		Depth[i] = value;
	}
}

Buffer::Buffer()
{
	width = 0;
	height = 0;
	CalculateLength();
	SetSize();
	setBackground();
	setDepth();
}

Buffer::Buffer(int width, int height)
{
	this->width = width;
	this->height = height;
	CalculateLength();
	SetSize();
	setBackground();
	setDepth();
}

Buffer::Buffer(int width, int height, RGB color)
{
	this->width = width;
	this->height = height;
	CalculateLength();
	SetSize();
	setBackground(color);
	setDepth();
}


Buffer::~Buffer()
{
}

int Buffer::GetWidth()
{
	return width;
}

int Buffer::GetHeight()
{
	return height;
}

void Buffer::SetWidth(int width)
{
	this->width = width;

}

void Buffer::SetHeight(int height)
{
	this->height = height;
}

int Buffer::GetLength()
{
	return length;
}

void Buffer::CalculateLength()
{
	length = width * height;
}

void Buffer::SetSize()
{
	VectorsColor.resize(length);
	Depth.resize(length);
}

void Buffer::SetColorInPixel(int i, int j, RGB color)
{
	int index = i + j * width;
	VectorsColor[index] = color;
}

void Buffer::SetDepth(int i, int j, float value)
{
	int index = i + j * width;
	Depth[index] = value;
}


RGB Buffer::GetColorInPixel(int i, int j)
{
	int index = i + j * width;
	return VectorsColor[index];
}

float Buffer::GetDepth(int i, int j)
{
	int index = i + j * width;
	return Depth[index];
}

