#include "pch.h"
#include "Vertex.h"


Vertex::Vertex()
{
	this->position = float3();
	this->normal = float3();
	this->color = RGB();
	this->UV = float3();
}

Vertex::Vertex(float3 position)
{
	this->position = position;
	this->normal = float3();
	this->color = RGB();
	this->UV = float3();

}

Vertex::Vertex(float3 position, float3 normal)
{
	this->position = position;
	this->normal = normal;
	this->color = RGB();
	this->UV = float3();
}

Vertex::Vertex(float3 position, float3 normal, RGB color)
{
	this->position = position;
	this->normal = normal;
	this->color = color;
	this->UV = float3();

}

Vertex::Vertex(float3 position, float3 normal, RGB color, float3 UV)
{
	this->position = position;
	this->normal = normal;
	this->color = color;
	this->UV = UV;
}


Vertex::~Vertex()
{
}

std::string Vertex::ToString()
{
	return "wierzcholek " + position.ToString() + " texture " + UV.ToString()+ " \n";
}
