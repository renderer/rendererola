#include "pch.h"
#include "Mesh.h"


Mesh::Mesh()
{
}


Mesh::~Mesh()
{
}

void Mesh::Draw(Rasterizer & r, VertexProcessor & vp, Light & light)
{
	for (int i = 0; i < meshTriangles.size(); i++)
	{
		meshTriangles.at(i).Draw(r, vp, light);
	}
}

void Mesh::Draw2(Rasterizer & r, VertexProcessor & vp, Light & light)
{
	for (int i = 0; i < meshTriangles.size(); i++)
	{
		meshTriangles.at(i).Draw2(r, vp, light);
	}
}

void Mesh::Draw3(Rasterizer & r, VertexProcessor & vp, std::vector<Light*> lights)
{
	for (int i = 0; i < meshTriangles.size(); i++)
	{
		meshTriangles.at(i).Draw3(r, vp, lights);
	}
}

void Mesh::Draw(Rasterizer & r, VertexProcessor & vp)
{
	for (int i = 0; i < meshTriangles.size(); i++)
	{
		meshTriangles.at(i).Draw(r, vp);
	}
}