#pragma once
#include "Light.h"

class SpotLight : public Light
{
public:
	float Angle;
	float3 Attenstions;
	float3 Direction;

	SpotLight(float3 position, RGB ambient, RGB diffuse, RGB specular, float3  direction, float angle, float3 attenstions);
	~SpotLight();

	virtual RGB calcuate(Vertex vertex, VertexProcessor& vp, Texture * texture = nullptr) override;
	virtual RGB calcuateOnlyColor(Vertex vertex, VertexProcessor & vp) override;
	virtual RGB calcuateOnlyColorSpecalur(Vertex vertex, VertexProcessor & vp) override;
	virtual float calcuateOnlyColorSpotLight(Vertex vertex, VertexProcessor & vp) override;
};

