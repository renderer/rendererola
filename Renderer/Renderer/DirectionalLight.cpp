#include "pch.h"
#include "DirectionalLight.h"



DirectionalLight::DirectionalLight(float3 position, RGB ambient, RGB diffuse, RGB specular) : Light(position, ambient, diffuse, specular)
{
}


DirectionalLight::~DirectionalLight()
{
}

RGB DirectionalLight::calcuate(Vertex vertex, VertexProcessor & vp, Texture * texture)
{
	float3 L = position.normalizeProduct();
	float3 normalRotate = vp.lt_obj2view(vertex.normal); //macierz3x3
	float3 N = normalRotate.normalizeProduct();

	float4 positionInView = (float4( vertex.position * (-1)).mul(vp.obj2view));
	float3 positionInViewFloat3 = float3(positionInView.GetX(), positionInView.GetY(), positionInView.GetZ());
	float3 V = positionInViewFloat3.normalizeProduct();

	float3 R = (L.reflect(N)).normalizeProduct();
	float diff = clamp01(L.dot(N));
	float aa = R.dot(V);
	float spec = pow(clamp01(R.dot(V)), 50);
	//float spec = aa > 0 ? aa : -aa;

	if (texture != nullptr)
	{
		RGB newtextureColor = texture->getColor(vertex.UV.getX(), vertex.UV.getY(),vertex.position);
		RGB abc = diffuse * diff;
		return (ambient + newtextureColor)* abc + (specular * spec);
		RGB aa = newtextureColor * RGB(clamp01(abc.getR()), clamp01(abc.getG()), clamp01(abc.getB())) + (specular * spec);
		return aa;
	}

	return ambient + diffuse * diff + specular * spec;
}


RGB DirectionalLight::calcuateOnlyColor(Vertex vertex, VertexProcessor & vp)
{
	float3 L = position.normalizeProduct();
	float3 normalRotate = vp.lt_obj2view(vertex.normal); //macierz3x3
	float3 N = normalRotate.normalizeProduct();

	float4 positionInView = (float4(vertex.position * (-1)).mul(vp.obj2view));
	float3 positionInViewFloat3 = float3(positionInView.GetX(), positionInView.GetY(), positionInView.GetZ());
	float3 V = positionInViewFloat3.normalizeProduct();

	float3 R = (L.reflect(N)).normalizeProduct();
	float diff = clamp01(L.dot(N));
	float aa = R.dot(V);
	float spec = pow(clamp01(R.dot(V)), 50);
	//float spec = aa > 0 ? aa : -aa;

	return ambient + diffuse * diff;
}

RGB DirectionalLight::calcuateOnlyColorSpecalur(Vertex vertex, VertexProcessor & vp)
{
	float3 L = position.normalizeProduct();
	float3 normalRotate = vp.lt_obj2view(vertex.normal); //macierz3x3
	float3 N = normalRotate.normalizeProduct();

	float4 positionInView = (float4(vertex.position * (-1)).mul(vp.obj2view));
	float3 positionInViewFloat3 = float3(positionInView.GetX(), positionInView.GetY(), positionInView.GetZ());
	float3 V = positionInViewFloat3.normalizeProduct();

	float3 R = (L.reflect(N)).normalizeProduct();
	float spec = pow(clamp01(R.dot(V)), 50);
	//float spec = aa > 0 ? aa : -aa;

	return  specular * spec;
}

float DirectionalLight::calcuateOnlyColorSpotLight(Vertex vertex, VertexProcessor & vp)
{
	return 1.0f;
}
