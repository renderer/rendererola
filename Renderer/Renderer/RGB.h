#pragma once
#include <iostream>
#include<string>

class RGB
{
protected:
	int r;
	int g;
	int b;

public:

	RGB();
	RGB(int value);
	RGB(int r, int g, int b);
	~RGB();
	float rToPixel();
	float gToPixel();
	float bToPixel();

	int getR();
	int getG();
	int getB();


	std::string toString();

	RGB operator *(float value);
	RGB operator *(RGB other);
	RGB operator /(float value);
	RGB operator /(RGB other);
	RGB operator +(RGB other);
	RGB operator -(RGB other);
	void operator *=(float value);
	void operator *=(RGB other);
	void operator /=(float value);
	void operator /=(RGB other);
	void operator +=(RGB other);
	void operator -=(RGB other);
	bool operator ==(RGB other);
	bool operator !=(RGB other);
	void clampColor01();
};

