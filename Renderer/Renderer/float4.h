#pragma once
#include <string>
#include <cmath>
class float4x4;
class float3;

class float4
{
	const float epsilon = 0.000001f;
public:
	float x, y, z, w;

	float4();
	float4(float _x, float _y, float _z, float _w);
	float4(float3 value);
	float4(float3 value, float w);
	~float4();

	float GetX();
	float GetY();
	float GetZ();
	float GetW();

	void SetX(float value);
	void SetY(float value);
	void SetZ(float value);
	void SetW(float value);

	float4 mul(float4x4 aa);
	float4 mul(float4x4 aa, float3 vector);
	std::string ToString();
	float length();
	float lengthSquared();
	float dot(float4 v);
	void negate();

	void add(float4 v);
	void sub(float4 v);
	void div(float f);
	void mag(float f);
	void normalize();
	float4 normalizeProduct();
	float4 reflect(float4 normal);
	float4 magProduct(float4 v, double f);
	float4 lerp(float4 v, double t);
	float4 toPoint();
	float4 operator*(double scalar);
	float4 operator*(float4 right);
	float4 operator+(float4 right);
	float4 operator-(float4 right);
	bool operator==(float4 right);
	bool operator!=(float4 right);
	float4 operator/(float scalar);
	float4 Reflect(float4 normal);
	float4 Reflect(float4 vec, float4 normal);
	float4& operator=(const float4 &vector);
};

