#include "pch.h"
#include "RGB.h"


RGB::RGB()
{
	this->r = 0.0;
	this->g = 0.0;
	this->b = 0.0;

}

RGB::RGB(int value)
{
	this->r = value > 255 ? 255 : (value < 0 ? 0 : value);
	this->g = value > 255 ? 255 : (value < 0 ? 0 : value);
	this->b = value > 255 ? 255 : (value < 0 ? 0 : value);
}

RGB::RGB(int r, int g, int b)
{
	this->r = r > 255 ? 255 : (r < 0 ? 0 : r);
	this->g = g > 255 ? 255 : (g < 0 ? 0 : g);
	this->b = b > 255 ? 255 : (b < 0 ? 0 : b);
}


RGB::~RGB()
{
}

float RGB::rToPixel()
{
	float temp = this->r * 255;
	if (temp > 255)
		return 255;
	else if (temp < 0)
		return 0;
	else
		return temp;

}

float RGB::gToPixel()
{
	float temp = this->g * 255;
	if (temp > 255)
		return 255;
	else if (temp < 0)
		return 0;
	else
		return temp;
}

float RGB::bToPixel()
{
	float temp = this->b * 255;
	if (temp > 255)
		return 255;
	else if (temp < 0)
		return 0;
	else
		return temp;
}

int RGB::getR()
{
	return r;
}

int RGB::getG()
{
	return g;
}

int RGB::getB()
{
	return b;
}


std::string RGB::toString()
{
	return "RBB(" + std::to_string(this->r) + ";" + std::to_string(this->g) + ";" + std::to_string(this->b) + ")";
}

RGB RGB::operator*(float value)
{
	int newR, newG, newB;
	newR = this->r * value;
	newG = this->g * value;
	newB = this->b * value;

	return RGB(newR, newG, newB);
}

RGB RGB::operator*(RGB other)
{
	int newR, newG, newB;
	newR = this->r * other.getR();
	newG = this->g * other.getG();
	newB = this->b * other.getB();

	int r = newR;
	int g = newG;
	int b = newB;

	newR = newR > 255 ? 255 : (newR < 0 ? 0 : newR);
	newG = newG > 255 ? 255 : (newG < 0 ? 0 : newG);
	newB = newB > 255 ? 255 : (newB < 0 ? 0 : newB);

	return RGB(newR, newG, newB);

}

RGB RGB::operator/(float value)
{
	if (value == 0.0f) return RGB();
	int newR, newG, newB;
	newR = this->r / value;
	newG = this->g / value;
	newB = this->b / value;

	return RGB(newR, newG, newB);
}

RGB RGB::operator/(RGB other)
{
	return RGB();
}

RGB RGB::operator+(RGB other)
{
	int newR, newG, newB;
	newR = this->r + other.r;
	newG = this->g + other.g;
	newB = this->b + other.b;

	return RGB(newR, newG, newB);
}

RGB RGB::operator-(RGB other)
{
	int newR, newG, newB;
	newR = this->r - other.r;
	newG = this->g - other.g;
	newB = this->b - other.b;

	return RGB(newR, newG, newB);
}

void RGB::operator*=(float value)
{

	int r = this->r *value;
	int g = this->g *value;
	int b = this->b *value;

	this->r = r > 255 ? 255 : (r < 0 ? 0 : r);
	this->g = g > 255 ? 255 : (g < 0 ? 0 : g);
	this->b = b > 255 ? 255 : (b < 0 ? 0 : b);

}

void RGB::operator*=(RGB other)
{
	float newR, newG, newB;
	newR = this->r * other.getR();
	newG = this->g * other.getG();
	newB = this->b * other.getB();

	int r = newR;
	int g = newG;
	int b = newB;

	this->r = r > 255 ? 255 : (r < 0 ? 0 : r);
	this->g = g > 255 ? 255 : (g < 0 ? 0 : g);
	this->b = b > 255 ? 255 : (b < 0 ? 0 : b);

}

void RGB::operator/=(float value)
{
	if (value == 0.0f) return;
	this->r = (int)this->r / value;
	this->g = int(this->g / value);
	this->b = this->b / value;

}

void RGB::operator/=(RGB other)
{
}

void RGB::operator+=(RGB other)
{
	int newR, newG, newB;
	newR = this->r + other.r;
	newG = this->g + other.g;
	newB = this->b + other.b;


	int r = newR;
	int g = newG;
	int b = newB;

	this->r = r > 255 ? 255 : (r < 0 ? 0 : r);
	this->g = g > 255 ? 255 : (g < 0 ? 0 : g);
	this->b = b > 255 ? 255 : (b < 0 ? 0 : b);
}

void RGB::operator-=(RGB other)
{
}

bool RGB::operator==(RGB other)
{
	if (this->r == other.r && this->g == other.g && this->b == other.b)
	{
		return true;
	}
	return false;
}

bool  RGB::operator !=(RGB other)
{
	if (this->r != other.r || this->g != other.g || this->b != other.b)
	{
		return true;
	}
	return false;
}


